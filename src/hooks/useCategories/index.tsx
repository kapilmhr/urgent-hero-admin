import React, { createContext, useContext } from 'react'
import { useFetch } from '../useFetch'
import { NormalizedObject } from '../../apis/types'
import { JobCategory } from '../../apis/jobCategories/index.d'
import { getCategories } from '../../apis/jobCategories'

const defaultValue: NormalizedObject<JobCategory> = {

}
interface CategoriesContext<T> {
  categories: T;
  error: any;
  isLoading: boolean;
}
const CatContext = createContext<CategoriesContext<NormalizedObject<JobCategory>>>({
  categories: defaultValue,
  error: null,
  isLoading: true

})

const useFetchCategory = (): CategoriesContext<NormalizedObject<JobCategory>> => {
  const { data, error, isLoading } = useFetch<NormalizedObject<JobCategory>>({
    initialData: defaultValue,
    reloadOn: [],
    fetchFunction: async () => {
      const [err, data] = await getCategories({ limit: 100, offset: 0 })
      if (err) {
        return [err, null]
      }
      let normalizedData = {}
      if (data) {
        data.forEach((category) => {
          normalizedData = { ...normalizedData, [category.id]: category }
        })
      }
      return [err, normalizedData]
    }
  })
  return {
    categories: data,
    error,
    isLoading
  }
}

export const CategoryProvider: React.FC = ({ children }) => {
  const user = useFetchCategory()
  return <CatContext.Provider value={user}>{children}</CatContext.Provider>
}
export const useCategory = () => {
  return useContext(CatContext)
}

/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
// Hook (use-auth.js)
import React, { useState, useEffect, useContext, createContext } from 'react'
import * as firebase from 'firebase/app'
import 'firebase/auth'
import { firebaseConfig } from '../../config'
import { setIdTokenToLocalStorage } from '../../utils/localStorage'

export interface AuthHook {
  user: firebase.User | null;
  wait: boolean;
  signin: (email: string, password: string) => Promise<firebase.User | null>;
  signup: (email: string, password: string) => Promise<firebase.User | null>;
  signout: () => Promise<void>;
  sendPasswordResetEmail: (email: string) => Promise<boolean>;
  confirmPasswordReset: (code: string, password: string) => Promise<boolean>;
}
// Add your Firebase credentials
firebase.initializeApp(firebaseConfig)

const AuthContext = createContext<AuthHook|{}>({})
// Provider hook that creates auth object and handles state
function useProvideAuth (): AuthHook {
  const [user, setUser] = useState<firebase.User | null>(null)
  const [wait, setWait] = useState<boolean>(true)
  // Wrap any Firebase methods we want to use making sure ...
  // ... to save the user to state.
  const signin = (email: string, password: string) => {
    console.log('signin')
    setWait(true)
    return firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(response => {
        setUser(response.user)
        setWait(false)
        return response.user
      })
  }

  const signup = (email: string, password: string) => {
    return firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(response => {
        setUser(response.user)
        return response.user
      })
  }

  const signout = () => {
    setWait(true)
    return firebase
      .auth()
      .signOut()
      .then(() => {
        setUser(null)
        setWait(false)
      })
  }

  const sendPasswordResetEmail = (email: string) => {
    return firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then(() => {
        return true
      })
  }

  const confirmPasswordReset = (code: string, password: string) => {
    return firebase
      .auth()
      .confirmPasswordReset(code, password)
      .then(() => {
        return true
      })
  }

  // Subscribe to user on mount
  // Because this sets state in the callback it will cause any ...
  // ... component that utilizes this hook to re-render with the ...
  // ... latest auth object.
  useEffect(() => {
    const unsubscribe = firebase.auth().onAuthStateChanged(user => {
      if (user) {
        user.getIdToken(false).then((token) => {
          setIdTokenToLocalStorage(token)
          setUser(user)
          setWait(false)
        })
      } else {
        setIdTokenToLocalStorage('')
        setUser(null)
        setWait(false)
      }
    })

    // Cleanup subscription on unmount
    return () => unsubscribe()
  }, [])

  // Return the user object and auth methods
  return {
    user,
    wait,
    signin,
    signup,
    signout,
    sendPasswordResetEmail,
    confirmPasswordReset
  }
}

// Provider component that wraps your app and makes auth object ...
// ... available to any child component that calls useAuth().
export function ProvideAuth ({ children }: any): any {
  const auth = useProvideAuth()
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>
}

// Hook for child components to get the auth object ...
// ... and re-render when it changes.
export const useAuth = (): AuthHook | {} => {
  return useContext(AuthContext)
}

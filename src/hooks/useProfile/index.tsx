import React, { createContext, useContext } from 'react'

import { User } from '../../apis/users/index.d'
import { getSelf } from '../../apis/users'
import { useFetch } from '../useFetch'
import { useAuth, AuthHook } from '../useAuth/useAuth'

const defaultValue: User = {
  id: '',
  uid: '',
  email: '',
  displayName: '',
  photoURL: '',
  phoneNumber: '',
  emailVerified: false,
  disabled: false,
  metadata: {
    lastSignInTime: '0',
    creationTime: '0'
  },
  providerData: [],
  tokensValidAfterTime: '',
  passwordHash: '',
  passwordSalt: '',
  tenantId: '',
  customClaims: {
    roles: []
  }
}
interface UserContext<T> {
  user: T;
  error: any;
  isLoading: boolean;
}
const UserContext = createContext<UserContext<User>>({
  user: defaultValue,
  error: null,
  isLoading: true

})

const useFetchUser = () => {
  const auth = useAuth() as AuthHook
  const { data, error, isLoading } = useFetch<User>({
    initialData: defaultValue,
    reloadOn: [auth.user],
    fetchFunction: async () => {
      if (auth.user) {
        const idToken = await auth.user.getIdToken(false)
        return await getSelf({ idToken })
      }
      return [null, null]
    }
  })
  return {
    user: data,
    error,
    isLoading
  }
}
export const ProfileProvider: React.FC = ({ children }) => {
  const user = useFetchUser()
  return <UserContext.Provider value={user}>{children}</UserContext.Provider>
}
export const useProfile = () => {
  return useContext(UserContext)
}

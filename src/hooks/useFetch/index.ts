import { useState, useEffect } from 'react'
import { AxiosFunctionReturnType } from '../../utils/asyncFunction'

interface UseFetchReturnType<T>{
  isLoading: boolean;
  data: T;
  error: any;
}
interface UseFetchParams<T>{
  initialData: T;
  reloadOn: Array<any>;
  fetchFunction: () => Promise<AxiosFunctionReturnType<T>>;
}

export const useFetch = <T>({ initialData, reloadOn, fetchFunction }: UseFetchParams<T>): UseFetchReturnType<T> => {
  const [isLoading, setLoading] = useState<boolean>(true)
  const [data, setData] = useState<T>(initialData)
  const [error, setError] = useState(null)
  useEffect(() => {
    const getData = async () => {
      setLoading(true)
      setData(initialData)
      const [err, data] = await fetchFunction()
      setLoading(false)
      if (data) {
        setData(data)
      }
      setError(err)
    }
    getData()
  }, reloadOn)
  return {
    isLoading,
    data,
    error
  }
}

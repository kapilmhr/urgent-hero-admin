import React from 'react'
import MainRoutes from '../routes/router'

const App: React.FC = () => {
  return (
    <MainRoutes/>
  )
}

export default App

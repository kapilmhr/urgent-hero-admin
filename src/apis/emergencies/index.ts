import { BaseParams, PaginationParams } from '../types'
import { asyncFunction, AxiosFunctionReturnType } from '../../utils/asyncFunction'
import { Emergency } from './types'
import { PaymentsChart } from '../payments/types'
import _ from 'lodash'

export type EmergencyParams = BaseParams

const BASE = '/emergencies'

export const getEmergencies = async ({ limit, offset }: PaginationParams) => {
  return await asyncFunction<Emergency[]>({
    method: 'GET',
    url: `${BASE}?filter[limit]=${limit || ''}&filter[skip]=${offset && limit ? limit * (offset - 1) : ''}`
  })
}
export const getAllEmergenciesForDashboard = async () => {
  const [err, response] = await asyncFunction<Emergency[]>({
    method: 'GET',
    url: `${BASE}`
  })
  if (response) {
    const groupedByCategory = _.groupBy(response, 'categoryId')
    const data: number[] = []
    Object.keys(groupedByCategory).forEach((category) => {
      data.push(groupedByCategory[category].length)
    })
    return [err, {
      labels: Object.keys(groupedByCategory) || [],
      data
    }] as AxiosFunctionReturnType<PaymentsChart>
  }
  return [err, response] as AxiosFunctionReturnType<PaymentsChart>
}

export const getEmergencyCount = async (status?: string) => {
  const [err, data] = await asyncFunction<{count: number}>({
    method: 'GET',
    url: `${BASE}/count${status ? `?where[status][eq]=${status}` : ''}`
  })
  return [err, data ? data.count : 0] as AxiosFunctionReturnType<number>
}

export const getFailedEmergencyCount = async () => {
  const [err, data] = await asyncFunction<{count: number}>({
    method: 'GET',
    url: `${BASE}/count?where[status][nin]=PENDING&where[status][nin]=CANCELED&where[status][nin]=COMPLETED&where[status][nin]=PENDING`
  })
  return [err, data ? data.count : 0] as AxiosFunctionReturnType<number>
}

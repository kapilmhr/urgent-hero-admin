import { Hero } from '../heros/types'
import { User } from '../users/index.d'
import { JobCategory } from '../jobCategories/index.d'
export interface Emergency {
  id: string;
  estimate: Estimate;
  status: string;
  description: string;
  categoryId: string;
  serviceLocation: Location;
  timestamp: number;
  heroId: string;
  userId: string;
  hero: Hero;
  user: User;
  category: JobCategory;
}


export interface BaseParams {
  id?: string;
  idToken?: string;
}

export interface NormalizedObject<T>{
  [id: string]: T;
}
export interface PaginationParams {
  limit?: number;
  offset?: number;
}

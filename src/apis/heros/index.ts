import { BaseParams, PaginationParams } from '../types'
import { asyncFunction, AxiosFunctionReturnType } from '../../utils/asyncFunction'
import { Hero } from './types'

export type HeroParams = BaseParams

const HERO_BASE = '/heroes'

export const getHeroApplication = async ({ limit, offset }: PaginationParams) => {
  return await asyncFunction<Hero[]>({
    method: 'GET',
    url: `${HERO_BASE}?filter[limit]=${limit || ''}&filter[skip]=${offset && limit ? limit * (offset - 1) : ''}`
  })
}

export const getApprovedHeros = async () => {
  return await asyncFunction<Hero[]>({
    method: 'GET',
    url: `${HERO_BASE}?filter[where][status][eq]=APPROVED`
  })
}

export const getHeroApplicationCount = async () => {
  const [err, data] = await asyncFunction<{count: number}>({
    method: 'GET',
    url: `${HERO_BASE}/count`
  })
  return [err, data ? data.count : 0] as AxiosFunctionReturnType<number>
}

export const getHeroById = async (id: string) => {
  return await asyncFunction<Hero>({
    method: 'GET',
    url: `${HERO_BASE}/${id}`
  })
}

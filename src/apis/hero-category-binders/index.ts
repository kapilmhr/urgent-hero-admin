import { asyncFunction } from '../../utils/asyncFunction'
import { HeroAndCategory, NewHeroAndCategory, BindedHeros } from './types'

export const bindHeroToCategory = async (data: NewHeroAndCategory) => {
  return await asyncFunction<HeroAndCategory>({
    method: 'PUT',
    url: '/hero-category-binders/bind',
    data
  })
}

export const rejectHero = async (data: NewHeroAndCategory) => {
  return await asyncFunction<HeroAndCategory>({
    method: 'POST',
    url: '/hero-category-binders/reject',
    data
  })
}

export const requestChanges = async (data: NewHeroAndCategory) => {
  return await asyncFunction<HeroAndCategory>({
    method: 'POST',
    url: '/hero-category-binders/changes',
    data
  })
}

export const listofHeros = async (binderId: string) => {
  return await asyncFunction<BindedHeros>({
    method: 'GET',
    url: `/hero-category-binders/${binderId}/heros`
  })
}

export const getBindedCategoryList = async () => {
  return await asyncFunction<HeroAndCategory[]>({
    method: 'GET',
    url: '/hero-category-binders'
  })
}

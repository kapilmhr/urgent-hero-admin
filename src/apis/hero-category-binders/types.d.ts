import { User } from '../users/index.d'

export interface NewHeroAndCategory{
  categoryId: string;
  userId: string[];
  userEmail: string[];
  emaildescription: string;
  applicationId: string;
}

export interface HeroAndCategory extends NewHeroAndCategory{
  id: string;
}

export interface BindedHeros {
  categoryId: string;
  heros: User[];
}

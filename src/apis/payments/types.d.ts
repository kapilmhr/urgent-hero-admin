export interface Payment{
  id: string;
  userId: string;
  heroId: string;
  categoryId: string;
  emergencyId: string;
  transaction: Transaction;
}

export interface Transaction{
  id: string;
  type: string;
  amount: number;
  time: number;
}

export interface TotalPayment {
  payments: Payment[];
  total: number;
  count: number;
}

export interface PaymentsChart {
  labels: string[];
  data: any;
}

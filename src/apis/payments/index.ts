import { asyncFunction, AxiosFunctionReturnType } from '../../utils/asyncFunction'
import { PaginationParams } from '../types'
import { Payment, TotalPayment, PaymentsChart } from './types'
import * as _ from 'lodash'
const BASE_ENDPOINT = '/payments'
export const getPayments = async ({ limit, offset }: PaginationParams) => {
  const [err, data] = await asyncFunction<Payment[]>({
    url: `${BASE_ENDPOINT}?filter[limit]=${limit || ''}&filter[skip]=${offset && limit ? limit * (offset - 1) : ''}`,
    method: 'GET'
  })
  if (data) {
    let total = 0
    data.forEach((payment) => {
      if (payment.transaction && payment.transaction.amount) {
        total += payment.transaction.amount
      }
    })
    return [err, {
      payments: data,
      total,
      count: data.length
    }] as AxiosFunctionReturnType<TotalPayment>
  }
  return [err, data] as AxiosFunctionReturnType<TotalPayment>
}

export const getAllPayments = async () => {
  const [err, data] = await asyncFunction<Payment[]>({
    url: `${BASE_ENDPOINT}`,
    method: 'GET'
  })
  if (data) {
    let total = 0
    data.forEach((payment) => {
      if (payment.transaction && payment.transaction.amount) {
        total += payment.transaction.amount
      }
    })
    return [err, {
      payments: data,
      total,
      count: data.length
    }] as AxiosFunctionReturnType<TotalPayment>
  }
  return [err, data] as AxiosFunctionReturnType<TotalPayment>
}
export interface PaymentUser{
  id: string;
  categoryId?: string;
}
export const getPaymentsOfUser = async ({ id, categoryId }: PaymentUser) => {
  const [err, data] = await asyncFunction<Payment[]>({
    url: `${BASE_ENDPOINT}/heroes/${id}`,
    method: 'GET',
    params: {
      categoryId
    }
  })
  if (data) {
    let total = 0
    data.forEach((payment) => {
      if (payment.transaction && payment.transaction.amount) {
        total += payment.transaction.amount
      }
    })
    return [err, {
      payments: data,
      total,
      count: data.length
    }] as AxiosFunctionReturnType<TotalPayment>
  }
  return [err, data] as AxiosFunctionReturnType<TotalPayment>
}

export const getPaymentsByCategoryId = async (id: string) => {
  const [err, data] = await asyncFunction<Payment[]>({
    url: `${BASE_ENDPOINT}/categories/${id}`,
    method: 'GET'
  })
  if (data) {
    let total = 0
    data.forEach((payment) => {
      if (payment.transaction && payment.transaction.amount) {
        total += payment.transaction.amount
      }
    })
    return [err, {
      payments: data,
      total,
      count: data.length
    }] as AxiosFunctionReturnType<TotalPayment>
  }
  return [err, data]as AxiosFunctionReturnType<PaymentsChart>
}
export const getAllPaymentsForDashboard = async () => {
  const [err, response] = await asyncFunction<Payment[]>({
    url: `${BASE_ENDPOINT}`,
    method: 'GET'
  })
  if (response) {
    const groupedByCategory = _.groupBy(response, 'categoryId')
    const data: number[] = []
    Object.keys(groupedByCategory).forEach((category) => {
      let total = 0
      groupedByCategory[category].forEach((cat) => {
        total += cat.transaction.amount
      })
      data.push(total)
    })
    return [err, {
      labels: Object.keys(groupedByCategory) || [],
      data
    }] as AxiosFunctionReturnType<PaymentsChart>
  }
  return [err, response] as AxiosFunctionReturnType<PaymentsChart>
}

import { BaseParams, PaginationParams } from '../types'
import { asyncFunction, AxiosFunctionReturnType } from '../../utils/asyncFunction'
import { HeroApplication } from './types'

export type HeroParams = BaseParams

const APPLICATION_BASE = '/hero-applications'

export const getApplications = async ({ limit, offset }: PaginationParams) => {
  return await asyncFunction<HeroApplication[]>({
    method: 'GET',
    url: `${APPLICATION_BASE}?filter[include][][relation]=hero&filter[limit]=${limit || ''}&filter[skip]=${offset && limit ? limit * (offset - 1) : ''}`
  })
}

export const getApplicationCount = async () => {
  const [err, data] = await asyncFunction<{count: number}>({
    method: 'GET',
    url: `${APPLICATION_BASE}/count`
  })
  return [err, data ? data.count : 0] as AxiosFunctionReturnType<number>
}

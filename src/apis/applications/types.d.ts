import { Hero } from '../heros/types'
import { ApplilcationStatus } from '../../routes/heroes/constants'

export interface HeroApplication{
  id: string;
  categoryId: string[];
  remarks: string;
  status: ApplilcationStatus;
  heroId: string;
  hero: Hero;
}

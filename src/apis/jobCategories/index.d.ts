// Generated by https://quicktype.io

export interface JobCategory {
  id: string;
  title: string;
  description: string;
  image: string;
}

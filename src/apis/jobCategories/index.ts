import { asyncFunction, AxiosFunctionReturnType } from '../../utils/asyncFunction'
import { BaseParams, PaginationParams } from '../types'
import { JobCategory } from './index.d'

const categoriesBase = '/categories'

export type JobCategoriesParams = BaseParams

export const getCategories = async ({ limit, offset }: PaginationParams) => {
  return await asyncFunction<JobCategory[]>({
    url: `${categoriesBase}?filter[limit]=${limit || ''}&filter[skip]=${offset && limit ? limit * (offset - 1) : ''}`,
    method: 'GET'
  })
}

export const createCategories = async (data: JobCategory) => {
  return await asyncFunction<JobCategory[]>({
    url: `${categoriesBase}`,
    method: 'POST',
    data
  })
}

export const uploadFile = async (files: File[]) => {
  const data = new FormData()
  files.forEach((file, index) => {
    data.append(`file-${index}`, file)
  })
  return await asyncFunction<JobCategory[]>({
    url: '/file/upload',
    method: 'POST',
    data
  })
}

export const getCategoriesCount = async () => {
  const [err, data] = await asyncFunction<{count: number}>({
    method: 'GET',
    url: `${categoriesBase}/count`
  })
  return [err, data ? data.count : 0] as AxiosFunctionReturnType<number>
}

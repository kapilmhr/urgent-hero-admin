import { RolesForSelect } from './index.d'

export const URGENT_HERO = 'URGENT_HERO'
export const URGENT_AGENT = 'URGENT_AGENT'
export const URGENT_LANDLORD = 'URGENT_LANDLORD'
export const URGENT_ADMIN = 'URGENT_ADMIN'
export const URGENT_UNBOARDING = 'URGENT_UNBOARDING'
export const URGENT_SUPER_ADMIN = 'URGENT_SUPER_ADMIN'
export const URGENT_USER = 'URGENT_USER'
export const roles = [URGENT_HERO, URGENT_AGENT, URGENT_LANDLORD, URGENT_UNBOARDING, URGENT_ADMIN, URGENT_SUPER_ADMIN]

export const rolesDisplayName = {
  [URGENT_HERO]: 'Hero',
  [URGENT_AGENT]: 'Agent',
  [URGENT_LANDLORD]: 'Landlord',
  [URGENT_ADMIN]: 'Admin',
  [URGENT_UNBOARDING]: 'Unboarding Team',
  [URGENT_SUPER_ADMIN]: 'Super Admin',
  [URGENT_USER]: 'User'
}

export const rolesListForSelect: RolesForSelect[] = [{
  label: rolesDisplayName[URGENT_HERO], value: URGENT_HERO
}, {
  label: rolesDisplayName[URGENT_ADMIN], value: URGENT_ADMIN
}, {
  label: rolesDisplayName[URGENT_AGENT], value: URGENT_AGENT
}, {
  label: rolesDisplayName[URGENT_LANDLORD], value: URGENT_LANDLORD
}, {
  label: rolesDisplayName[URGENT_UNBOARDING], value: URGENT_UNBOARDING
}, {
  label: rolesDisplayName[URGENT_SUPER_ADMIN], value: URGENT_SUPER_ADMIN
}, {
  label: rolesDisplayName[URGENT_USER], value: URGENT_USER
}]
export const rolesColorMap = {
  [URGENT_HERO]: 'badge-primary',
  [URGENT_AGENT]: 'badge-info',
  [URGENT_LANDLORD]: 'badge-success',
  [URGENT_ADMIN]: 'badge-warning',
  [URGENT_UNBOARDING]: 'badge-warning',
  [URGENT_SUPER_ADMIN]: 'badge-danger',
  [URGENT_USER]: 'badge-info'
}

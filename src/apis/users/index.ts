import { asyncFunction, AxiosFunctionReturnType } from '../../utils/asyncFunction'
import { User } from './index.d'
import { BaseParams, PaginationParams } from '../types'
const userBaseUrl = '/users'

interface BaseGetUsers extends PaginationParams{
  additionalQuery?: string;
}
const baseGetUsers = async ({ limit, offset, additionalQuery }: BaseGetUsers) => {
  return await asyncFunction<User[]>({
    url: `${userBaseUrl}?filter[limit]=${limit || ''}&filter[skip]=${offset && limit ? limit * (offset - 1) : ''}${additionalQuery ? `&${additionalQuery}` : ''}`,
    method: 'GET'
  })
}
export const getUsers = async (params: PaginationParams) => {
  return await baseGetUsers(params)
}
export const getAdmins = async (params: PaginationParams) => {
  const additionalQuery = 'filter[where][customClaims.roles][inq]=URGENT_ADMIN&filter[where][customClaims.roles][inq]=URGENT_SUPER_ADMIN&filter[where][customClaims.roles][inq]=URGENT_UNBOARDING'
  return await baseGetUsers({ ...params, additionalQuery })
}
export const getSelf = async ({ idToken }: BaseParams) => {
  return await asyncFunction<User>({
    url: `${userBaseUrl}/me`,
    method: 'POST',
    headers: {
      Authorization: `Bearer ${idToken}`
    }
  })
}
export const getUsersCount = async () => {
  const [err, data] = await asyncFunction<{count: number}>({
    url: `${userBaseUrl}/count`,
    method: 'GET'
  })
  return [err, data ? data.count : 0] as AxiosFunctionReturnType<number>
}
// export const createUser = async (params: PaginationParams) => {
//   const [err, data] = await asyncFunction<User>({
//     url: `${userBaseUrl}`,
//     method: 'POST'
//   })
//   console.log(err, data)
// }

export const getUsersById = async ({ id }: BaseParams) => {
  const [err, data] = await asyncFunction<User>({
    url: `${userBaseUrl}/${id}`,
    method: 'GET'
  })
  console.log(err, data)
}

export const assignRoles = async (userId: string, roles: string[]) => {
  return await asyncFunction<null>({
    url: `${userBaseUrl}/${userId}/roles`,
    method: 'PATCH',
    data: {
      roles
    }
  })
}

interface GetUsersByEmail extends PaginationParams {
  email?: string;
}
export const getUsersByEmail = async ({ email, ...rest }: GetUsersByEmail) => {
  const additionalQuery = `filter[where][email][like]=${email || ''}`
  return await baseGetUsers({ ...rest, additionalQuery })
}

import axios from 'axios'
import { showError } from '../utils/notification'
import { baseAPI } from '../config'

const instance = axios.create({
  baseURL: baseAPI
  // timeout: 10000
})

instance.interceptors.response.use((response) => {
  return response
}, (error) => {
  if (error.response && error.response.data) {
    showError(error.response.data.error.message || error.response.statusText)
    if (error.response.data.error.name === 'UnauthorizedError') {
      window.location.href = '/login'
    }
    throw error.response.data
  }
  if (error.message === 'Network Error') {
    showError('Failed to connect to server')
    setTimeout(() => { window.location.href = '/logout' }, 2000)
  }
  throw error
})

export default instance

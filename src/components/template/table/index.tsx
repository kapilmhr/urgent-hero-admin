/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState } from 'react'
import Spinner from '../../bootstrap/spinner'
import { Table } from 'react-bootstrap'
import UrgentPagination from './pagination'
import { AxiosFunctionReturnType } from '../../../utils/asyncFunction'
import { useFetch } from '../../../hooks/useFetch'
import { PaginationParams } from '../../../apis/types'
export interface TableHeader{
  name: string;
  dataAttribute: string;
  formatter?: (data: any, index: number) => JSX.Element|JSX.Element[]|string | null | undefined;
}

export interface TableProps {
  headers: TableHeader[];
  getData: (params: PaginationParams) => Promise<AxiosFunctionReturnType<any>>;
  getCount: () => Promise<AxiosFunctionReturnType<number>>;
  hidePagination?: boolean;
}
const displayData = (head: TableHeader, data: any, index: number) => {
  if (head.formatter) {
    return head.formatter(data, index)
  }
  return data[head.dataAttribute] || '-'
}
const CustomTable: React.FC<TableProps> = ({ headers, getData, getCount, hidePagination }) => {
  const [currentPage, setCurrentPage] = useState<number>(1)
  const [currentSize, setCurrentSize] = useState<number>(10)
  const { data, isLoading } = useFetch<any[]>({
    initialData: [],
    reloadOn: [currentPage, currentSize],
    fetchFunction: async () => {
      return await getData({ limit: currentSize, offset: currentPage })
    }
  })
  const getDataNumber = (rowIndex: number) => {
    return currentSize * (currentPage - 1) + (rowIndex + 1)
  }
  return (
    <>
      <div className="table-responsive">
        <Table hover striped className="table table-head-bg-primary">
          <thead>
            <tr>
              <th>S.N.</th>
              {headers.map((header, index) => (
                <th scope="col" key={`${header.name}-${index}`}>{header.name}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {data.map((row: any, rowIndex: number) => {
              return (
                <tr key={`${row.id}-${rowIndex}`}>
                  <td>{getDataNumber(rowIndex)}</td>
                  {headers.map((head, dataIndex) => (
                    <td key={`${head.dataAttribute}-${dataIndex}`}>{displayData(head, row, rowIndex)}</td>
                  ))}
                </tr>
              )
            })}
          </tbody>
        </Table>
      </div>
      {isLoading && <Spinner/>}
      {data.length === 0 && !isLoading && <div className="text-center">No Data</div>}
      {!hidePagination && <UrgentPagination handlePageChange={setCurrentPage} hanldeSizeChange={setCurrentSize} getCount={getCount} currentPage={currentPage} currentSize={currentSize}/> }
    </>
  )
}

export default CustomTable

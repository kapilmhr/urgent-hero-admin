import React, { useEffect, useState } from 'react'
import Pagination from 'react-js-pagination'
import { useFetch } from '../../../../hooks/useFetch'
import { AxiosFunctionReturnType } from '../../../../utils/asyncFunction'

interface PaginationProps{
  handlePageChange: (page: number) => void;
  hanldeSizeChange: (size: number) => void;
  currentPage: number;
  currentSize: number;
  getCount: () => Promise<AxiosFunctionReturnType<number>>;
}
const UrgentPagination: React.FC<PaginationProps> = (props) => {
  const [total, setTotal] = useState<number>(0)

  const count = useFetch<number>({
    initialData: 0,
    reloadOn: [],
    fetchFunction: async () => {
      return await props.getCount()
    }
  })
  useEffect(() => {
    setTotal(count.data)
  }, [count])

  return (
    <Pagination
      innerClass="pagination pg-primary float-right"
      itemClass="page-item"
      linkClass="page-link"
      activeClass="active"
      activePage={props.currentPage}
      itemsCountPerPage={props.currentSize}
      totalItemsCount={total}
      onChange={props.handlePageChange}
    />
  )
}

export default UrgentPagination

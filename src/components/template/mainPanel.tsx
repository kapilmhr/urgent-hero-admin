import React from 'react'

interface MainPanelProps{
  title: string;
}
const MainPanel: React.FC<MainPanelProps> = ({ children, title }) => {
  return (
    <div className="main-panel">
      <div className="content">
        <div className="container-fluid">
          <h4 className="page-title">
            {title}
          </h4>
          {children}
        </div>
      </div>
    </div>
  )
}
export default MainPanel

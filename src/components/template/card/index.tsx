import React from 'react'

interface CardProps{
  title?: string;
  subtitle?: string;
  additionalClass?: string;
  icon?: string;
}
const Card: React.FC<CardProps> = ({ children, title, subtitle, additionalClass, icon }) => {
  return (
    <div className={`card ${additionalClass}`}>
      {title
        ? <div className="card-header">
          <h4 className="card-title"> <i className={`la la-${icon} text-primary`}/>{' '}{title}</h4>
          {subtitle && <p className="card-category">{subtitle}</p> }
        </div>
        : null}
      <div className="card-body">
        {children}
      </div>
    </div>
  )
}

export default Card

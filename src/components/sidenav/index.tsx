import React from 'react'
import { Link } from 'react-router-dom'
import { useRouter } from '../../hooks/useRouter'
import { Role } from '../../apis/users/index.d'
import { useProfile } from '../../hooks/useProfile'
import { URGENT_UNBOARDING, URGENT_ADMIN, URGENT_SUPER_ADMIN } from '../../apis/users/constants'
interface NavItem{
  title: string;
  icon: string;
  url: string;
  allowedRole: Role;
}
const navItems: NavItem[] = [
  {
    title: 'Dashboard',
    icon: 'la la-dashboard',
    url: 'dashboard',
    allowedRole: URGENT_UNBOARDING
  },
  {
    title: 'Job Categories',
    icon: 'la la-gears',
    url: 'categories',
    allowedRole: URGENT_ADMIN
  },
  {
    title: 'Users',
    icon: 'la la-users',
    url: 'users',
    allowedRole: URGENT_UNBOARDING
  },
  {
    title: 'Role Management',
    icon: 'la la-user-plus',
    url: 'roles',
    allowedRole: URGENT_SUPER_ADMIN
  },

  {
    title: 'Heroes',
    icon: 'la la-certificate',
    url: 'heroes/applications',
    allowedRole: URGENT_UNBOARDING
  }
]
interface SideNavProps {
  toggleSideBar: () => void;
}
const SideNav: React.FC<SideNavProps> = () => {
  const router = useRouter()
  const { user } = useProfile()
  return (
    <div className="sidebar">
      <div className="scrollbar-inner sidebar-wrapper">
        {/* <div className="user">
          <div className="photo">
            <img src="assets/img/profile.jpg"/>
          </div>
          <div className="info">
            <a className="" data-toggle="collapse" href="#collapseExample" aria-expanded="true">
              <span>Hizrian
                <span className="user-level">Administrator</span>
                <span className="caret"></span>
              </span>
            </a>
            <div className="clearfix"></div>

            <div className="collapse in" id="collapseExample" aria-expanded="true">
              <ul className="nav">
                <li>
                  <a href="#profile">
                    <span className="link-collapse">My Profile</span>
                  </a>
                </li>
                <li>
                  <a href="#edit">
                    <span className="link-collapse">Edit Profile</span>
                  </a>
                </li>
                <li>
                  <a href="#settings">
                    <span className="link-collapse">Settings</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
         */}
        <ul className="nav">
          {
            navItems.map((item, index) => {
              if (user.customClaims.roles.includes(item.allowedRole)) {
                return (
                  <li className={`nav-item ${router.pathname.includes(item.url) ? 'active' : ''}`} key={index}>
                    <Link to={`/${item.url}`}>
                      <i className={item.icon}/>
                      <p>{item.title}</p>
                      {/* <span className="badge badge-count">6</span> */}
                    </Link>
                  </li>
                )
              }
              return null
            })
          }
        </ul>
      </div>
    </div>
  )
}

export default SideNav

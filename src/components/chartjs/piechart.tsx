import React, { useRef, useEffect, useState } from 'react'
import { ChartBase } from './types'
import Chart from 'chart.js'
import { useCategory } from '../../hooks/useCategories'

const PieChart: React.FC<ChartBase> = ({ id, configuration, getData }) => {
  const { categories } = useCategory()
  const [data, setData] = useState<number[]>([])
  const [labels, setLabel] = useState<string[]>([])
  useEffect(() => {
    const fetchData = async () => {
      const [, data] = await getData()
      if (data) {
        setData(data.data)
        const mappedLabels: string[] = data.labels.map((id) => {
          return categories[id] ? categories[id].title : 'Unknown'
        })
        setLabel(mappedLabels)
      }
    }
    if (categories) {
      fetchData()
    }
  }, [categories])
  const canvasRef = useRef<HTMLCanvasElement | null>(null)
  useEffect(() => {
    if (canvasRef && canvasRef.current) {
      const ctx = canvasRef.current.getContext('2d')
      if (ctx) {
        const myChart = new Chart(ctx, {
          type: 'pie',
          data: {
            labels,
            datasets: [{
              data,
              backgroundColor: [
                'rgba(255, 99, 132, 1)',
                '#46b0f2',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
              ],
              borderColor: [
                'rgba(255, 99, 132, 1)',
                '#46b0f2',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
            }]
          },
          ...configuration
        })
        return () => {
          myChart.destroy()
        }
      }
    }
  }, [data, labels])
  return (
    <canvas ref={canvasRef} id={id}/>
  )
}

export default PieChart

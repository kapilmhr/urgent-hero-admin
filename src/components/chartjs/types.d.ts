import { AxiosFunctionReturnType } from '../../utils/asyncFunction'
import { PaymentsChart } from '../../apis/payments/types'

export interface ChartBase {
  id: string;
  getData: () => Promise<AxiosFunctionReturnType<PaymentsChart>>;
  configuration?: Chart.ChartConfiguration;
  // getData: () => Promise<ChartData>;
}

export interface ChartData{
  [id: string]: any;
}

import React from 'react'
import { CRouteComponentProps } from '../../@types/route'
import { Link } from 'react-router-dom'
import { LOGIN_ROUTE, LOGOUT_ROUTE, UNAUTHORIZED_ROUTE } from '../../routes/constants'

interface FourOFourProps extends CRouteComponentProps{
  message?: string;
}
const FourOFour: React.FC<FourOFourProps> = ({ message, location }) => {
  const { pathname } = location
  return (
    <div className="full-page-container">
      <div style={{ margin: '0 auto' }}><h2>404 - Page not found</h2>
        {message ? <p dangerouslySetInnerHTML={{ __html: message }}/> : <p>The page you are looking for might have been removed had its name changed or is temporarily unavailable.</p> }
        {!pathname.includes(UNAUTHORIZED_ROUTE) && <Link to={LOGIN_ROUTE} className="btn btn-primary mr-3">Go Home</Link>}
        <Link to={LOGOUT_ROUTE} className="btn btn-danger">Logout</Link>
      </div>
    </div>
  )
}

export default FourOFour

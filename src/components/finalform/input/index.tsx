import React from 'react'
import { Field } from 'react-final-form'
import { FormGroup, FormGroupSelect, Option } from '../../bootstrap/input'

interface FinalInput {
  name: string;
  title?: string;
  type?: string;
  disabled?: boolean;
  className?: string;
  formGroupClass?: string;
  required?: boolean;
  placeholder?: string;
}
interface SelectInput extends FinalInput{
  options: Option[];
}
export const FinalInput: React.FC<FinalInput> = ({ name, title, type, disabled, formGroupClass, required, placeholder }) => {
  return (
    <Field name={name} validate={required ? (value) => (!value || !value.trim() ? 'Required' : undefined) : undefined}>
      {({ input, meta }) => (
        <>
          <FormGroup title={title} placeholder={placeholder} {...input} type={type} disabled={disabled} formGroupClass={formGroupClass}/>
          {meta.touched && meta.error && <span>{meta.error}</span>}
        </>
      )}
    </Field>
  )
}
export const FinalSelect: React.FC<SelectInput> = ({ name, title, type, disabled, formGroupClass, required, options }) => {
  return (
    <Field name={name} validate={required ? (value) => (!value || !value.trim() ? 'Required' : undefined) : undefined}>
      {({ input, meta }) => (
        <>
          <FormGroupSelect options={options} title={title} {...input} type={type} disabled={disabled} formGroupClass={formGroupClass}/>
          {meta.touched && meta.error && <span>{meta.error}</span>}
        </>
      )}
    </Field>
  )
}
export const FinalText: React.FC<FinalInput> = ({ name, title, type, disabled, className, formGroupClass }) => {
  return (
    <Field name={name}>
      {({ input, meta }) => (
        <>
          <FormGroup title={title} {...input} type={type} disabled={disabled} className={`form-control-plaintext ${className || ''}`} formGroupClass={formGroupClass}/>
          {meta.touched && meta.error && <span>{meta.error}</span>}
        </>
      )}
    </Field>
  )
}

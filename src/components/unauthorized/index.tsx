import React from 'react'
import { LOGOUT_ROUTE } from '../../routes/constants'
import { Link } from 'react-router-dom'

const Unauthorized: React.FC = () => {
  return (
    <div className="full-page-container">
      <div style={{ margin: '0 auto' }}><h2>Unauthorized</h2>
        <p>You don&apos;t have required roles. If this is an error, contact Administrator.</p>
        <Link to={LOGOUT_ROUTE} className="btn btn-danger">Logout</Link>
      </div>
    </div>
  )
}

export default Unauthorized

import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { LOGOUT_ROUTE, HOME_ROUTE } from '../../routes/constants'
import { useProfile } from '../../hooks/useProfile'

interface HeaderProps {
  toggleSideBar: () => void;
  toggleTopBar: () => void;
  navOpen: boolean;
  headerOpen: boolean;
}
const Header: React.FC<HeaderProps> = ({ toggleSideBar, toggleTopBar, navOpen, headerOpen }) => {
  const [showProfileDropdown, setShowProfileDropdown] = useState<boolean>(false)

  const dropDownToggler = React.createRef<any>()
  const handleProfileClick = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    e.preventDefault()
    setShowProfileDropdown(!showProfileDropdown)
  }
  const shouldDismiss = (e: any) => {
    if (dropDownToggler.current) {
      return !dropDownToggler.current.contains(e.target)
    }
    return false
  }
  const dismissDropDown = (e: any) => {
    if (shouldDismiss(e) && showProfileDropdown) {
      setShowProfileDropdown(false)
    }
  }

  useEffect(() => {
    document.addEventListener('click', dismissDropDown)
    return () => document.removeEventListener('click', dismissDropDown)
  })
  const { user } = useProfile()

  return (
    <div className="main-header">
      <div className="logo-header">
        <Link to={HOME_ROUTE} className="logo">Urgent Hero</Link>
        <button onClick={toggleSideBar} className={`navbar-toggler sidenav-toggler ml-auto ${navOpen ? 'toggled' : ''}`} type="button" data-toggle="collapse" data-target="collapse" aria-controls="sidebar" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <button onClick={toggleTopBar} className={`topbar-toggler more ${headerOpen ? 'toggled' : ''}`}><i className="la la-ellipsis-v"></i></button>
      </div>
      <nav className="navbar navbar-header navbar-expand-lg">
        <div className="container-fluid">

          {/* <form className="navbar-left navbar-form nav-search mr-md-3" action="">
            <div className="input-group">
              <input type="text" placeholder="Search ..." className="form-control"/>
              <div className="input-group-append">
                <span className="input-group-text">
                  <i className="la la-search search-icon"/>
                </span>
              </div>
            </div>
          </form> */}
          <ul ref={dropDownToggler} className="navbar-nav topbar-nav ml-md-auto align-items-center">
            <li className={`nav-item dropdown ${showProfileDropdown ? 'show' : ''}`}>
              <a className="dropdown-toggle profile-pic" onClick={handleProfileClick} href="#" aria-expanded={showProfileDropdown}>
                {/* <img src={auth.user ? auth.user.photoURL ? auth.user.photoURL : './images/user.svg' : './images/user.svg'} alt="user-img" width="36" className="img-circle"/> */}
                <span >{user.displayName}</span></a>
              <ul className={`dropdown-menu dropdown-user ${showProfileDropdown ? 'show' : ''}`}>
                <li>
                  <div className="user-box" id="profile-drop-down">
                    {/* <div className="u-img"><img src="assets/img/profile.jpg" alt="user"/></div> */}
                    <div className="u-text">
                      <h4>{user.displayName}</h4>
                      <p className="text-muted">{user.email}</p>
                      {/* <a href="profile.html" className="btn btn-rounded btn-danger btn-sm">View Profile</a> */}
                    </div>
                  </div>
                </li>
                {/* <div className="dropdown-divider"></div>
                <a className="dropdown-item" href="#"><i className="ti-user"></i> My Profile</a>
                <div className="dropdown-divider"></div>
                <a className="dropdown-item" href="#"><i className="ti-settings"></i> Account Setting</a> */}
                <div className="dropdown-divider"></div>
                <Link to={LOGOUT_ROUTE} className="dropdown-item"><i className="fa fa-power-off"/> Logout</Link>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  )
}

export default Header

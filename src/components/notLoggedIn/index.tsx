import React from 'react'
import { LOGIN_ROUTE } from '../../routes/constants'
import { Link } from 'react-router-dom'

const NotLoggedIn: React.FC = () => {
  return (
    <div className="full-page-container">
      <div style={{ margin: '0 auto' }}><h2>Not Logged in</h2>
        <p>Login to continue.</p>
        <Link to={LOGIN_ROUTE} className="btn btn-success">Login</Link>
      </div>
    </div>
  )
}

export default NotLoggedIn

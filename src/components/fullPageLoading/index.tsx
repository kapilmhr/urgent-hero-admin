import React from 'react'
import Spinner from '../bootstrap/spinner'

const FullPageLoading: React.FC = () => (<div className="full-page-container"><div style={{ margin: '0 auto' }}><Spinner/></div></div>)

export default FullPageLoading

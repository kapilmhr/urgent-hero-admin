import React from 'react'
import { RouteProps, Route, Redirect } from 'react-router-dom'
import { useAuth, AuthHook } from '../../hooks/useAuth/useAuth'
import FullPageLoading from '../fullPageLoading'
import { NOT_LOGGED_IN_ROUTE } from '../../routes/constants'

const LoggedInRoute: React.FC<RouteProps> = (props) => {
  const auth = useAuth() as AuthHook
  if (auth.wait) {
    return <FullPageLoading/>
  }
  if (auth.user) {
    return <Route {...props}/>
  }
  return <Redirect to={NOT_LOGGED_IN_ROUTE}/>
}

export default LoggedInRoute

import React from 'react'
import { Link } from 'react-router-dom'
import { useRouter } from '../../../hooks/useRouter'

export interface Tab{
  title: string;
  url: string;
  // icon?: string;
}
interface NavTabProps{
  tabs: Tab[];
}
export const NavTab: React.FC<NavTabProps> = ({ tabs }) => {
  const { location } = useRouter()
  return (
    <ul className="nav nav-tabs nav-justified">
      {tabs.map((tab, index) => {
        return (
          <li className="nav-item" key={`${tab.url}-${index}`}>
            <Link role="button" to={tab.url} className={`nav-link ${location.pathname === tab.url ? 'active' : ''}`}>{tab.title}</Link>
          </li>
        )
      })}
    </ul>
  )
}

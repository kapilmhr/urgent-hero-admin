import React from 'react'

interface ButtonProps{
  title: string;
  type?: 'submit' | 'reset' | 'button';
  className?: string;
  onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  disabled?: boolean;
}
const Button: React.FC<ButtonProps> = ({ title, className, type, onClick, disabled }) => {
  return (
    <button type={type || 'button'} className={`${className}`} onClick={onClick} disabled={disabled}>{title}</button>
  )
}
export const ButtonPrimary: React.FC<ButtonProps> = ({ className, ...rest }) => {
  return (
    <Button className={`btn btn-primary ${className}`} {...rest}/>
  )
}

export const ButtonSuccess: React.FC<ButtonProps> = ({ className, ...rest }) => {
  return (
    <Button className={`btn btn-success ${className}`} {...rest}/>
  )
}

export const ButtonDanger: React.FC<ButtonProps> = ({ className, ...rest }) => {
  return (
    <Button className={`btn btn-danger ${className}`} {...rest}/>
  )
}

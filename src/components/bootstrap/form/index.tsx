import React from 'react'

interface FormProps {
  onSubmit?: (e: React.FormEvent<HTMLFormElement>) => void;
  className?: string;
}
const HTMLForm: React.FC<FormProps> = (props) => {
  const { onSubmit, children, className } = props
  return (
    <form onSubmit={onSubmit} className={className}>
      {children}
    </form>
  )
}

export default HTMLForm

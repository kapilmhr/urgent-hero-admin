import React from 'react'
import { Modal } from 'react-bootstrap'
interface ModalProps{
  show: boolean;
  toggleModal: () => void;
  title: string;
  size?: 'sm' | 'lg' | 'xl';
  className?: string;
  scrollable?: boolean;
}
const CustomModal: React.FC<ModalProps> = ({ show, toggleModal, children, title, size, className, scrollable }) => {
  return (
    <Modal
      size={size || 'xl'}
      show={show}
      onHide={toggleModal}
      backdrop="static"
      scrollable={scrollable}
      aria-labelledby="modal"
      dialogClassName={className}
    >
      <Modal.Header closeButton>
        <Modal.Title id="modalTitle">
          {title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>{children}</Modal.Body>
    </Modal>
  )
}

export default CustomModal

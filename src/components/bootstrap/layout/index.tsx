import React from 'react'

export const Container: React.FC<{className?: string}> = ({ children, className }) => {
  return (
    <div className={`container-fluid ${className || ''}`}>
      {children}
    </div>
  )
}

export const Row: React.FC<{className?: string}> = ({ children, className }) => {
  return (
    <div className={`row ${className || ''}`}>
      {children}
    </div>
  )
}

interface ColumnProps{
  className?: string;
}
export const Column: React.FC<ColumnProps> = ({ children, className }) => {
  return (
    <div className={className || 'col'}>
      {children}
    </div>
  )
}

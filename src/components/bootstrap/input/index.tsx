import React from 'react'

interface InputProps{
  className?: string;
  type?: string;
  placeholder?: string;
  id?: string;
  value?: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => void;
  disabled?: boolean;
}
interface InputFile{
  className: string;
  id: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement >) => void;
}
export const Input: React.FC<InputProps> = (props) => {
  return (
    <input {...props}/>
  )
}

export const InputLarge: React.FC<InputProps> = ({ className, ...rest }) => {
  return (
    <Input className={`form-control-lg ${className}`} {...rest}/>
  )
}

export const FileInput: React.FC<InputFile> = ({ className, id, onChange }) => {
  return (
    <input type={'file'} className={`form-control-file ${className}`}id={id} onChange={onChange}/>
  )
}

interface FormGroupProps extends InputProps{
  title?: string;
  helpText?: string;
  disabled?: boolean;
  formGroupClass?: string;
}

export interface Option{
  label: string;
  value: string;
}
interface FormGroupSelectProps extends FormGroupProps{
  options: Option[];
}

export const FormGroupBase: React.FC<{className?: string; title?: string; htmlFor?: string}> = ({ className, children, htmlFor, title }) => {
  return (
    <div className={`form-group ${className || ''}`}>
      {title && <label htmlFor={htmlFor}>{title}</label>}
      {children}
    </div>
  )
}
export const FormGroup: React.FC<FormGroupProps> = ({ helpText, title, className, formGroupClass, ...rest }) => {
  return (
    <FormGroupBase className={formGroupClass} htmlFor={rest.id} title={title}>
      <Input {...rest} className={className || 'form-control'}/>
      {helpText ? <small id={`${rest.id ? `${rest.id}-help` : ''}`} className={'form-text text-muted'}>{helpText}</small> : null}
    </FormGroupBase>
  )
}

export const FormGroupSelect: React.FC<FormGroupSelectProps> = ({ helpText, title, className, formGroupClass, options, ...rest }) => {
  return (
    <FormGroupBase className={formGroupClass} htmlFor={rest.id} title={title}>
      <select {...rest} className={className || 'form-control'} >
        {options.map((option) => {
          return (
            <option key={option.value} value={option.value}>{option.label}</option>
          )
        })}
      </select>
      {helpText ? <small id={`${rest.id ? `${rest.id}-help` : ''}`} className={'form-text text-muted'}>{helpText}</small> : null}
    </FormGroupBase>
  )
}
export const FormRow: React.FC = ({ children }) => {
  return (
    <div className="form-row">
      {children}
    </div>
  )
}

interface FormTextProps{
  value: string;
  title: string;
  className: string;
  helpText?: string;
}
export const FormText: React.FC<FormTextProps> = ({ title, value, className, helpText }) => {
  return (
    <FormGroup title={title} value={value} type={'text'} className={'form-control-plaintext'} formGroupClass={className} helpText={helpText}/>
  )
}

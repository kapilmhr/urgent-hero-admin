import React from 'react'

const SmallSpinner: React.FC<{variant: string}> = ({ variant }) => {
  return (
    <div className={`spinner-grow text-${variant}`} role="status">
      <span className="sr-only">Loading...</span>
    </div>
  )
}

export default SmallSpinner

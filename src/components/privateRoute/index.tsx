import React from 'react'
import { RouteProps, Route, Redirect } from 'react-router-dom'
import { useAuth, AuthHook } from '../../hooks/useAuth/useAuth'
import { LOGIN_ROUTE } from '../../routes/constants'
import { useProfile } from '../../hooks/useProfile'
import { Role } from '../../apis/users/index.d'
import FourOFour from '../404'
import { useRouter } from '../../hooks/useRouter'
import FullPageLoading from '../fullPageLoading'
import { rolesDisplayName } from '../../apis/users/constants'

interface PrivateRouteProps extends RouteProps {
  role: Role;
}
const PrivateRoute: React.FC<PrivateRouteProps> = ({ role, ...props }) => {
  const auth = useAuth() as AuthHook
  const { location, history, match } = useRouter()
  const { user, isLoading } = useProfile()
  if (auth.user) {
    if (isLoading) { return <FullPageLoading/> }
    if (user.customClaims.roles.includes(role)) { return <Route {...props}/> }
    return <FourOFour location={location} history={history} match={match} message={`You don't have <strong>${rolesDisplayName[role]}</strong> role to view this page.`}/>
  }
  return <Redirect to={{ pathname: LOGIN_ROUTE, state: { from: props.path } }}/>
}

export default PrivateRoute

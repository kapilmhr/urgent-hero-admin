import React, { useState, useEffect } from 'react'
import Header from '../header'
import SideNav from '../sidenav'
import { useAuth, AuthHook } from '../../hooks/useAuth/useAuth'

const WithHeaderAndSideNav: React.FC = ({ children }) => {
  const [navOpen, setNavOpen] = useState<boolean>(false)
  const [headerOpen, setHeaderOpen] = useState<boolean>(false)
  const toggleSideBar = () => {
    setNavOpen(!navOpen)
  }
  const toggleTopBar = () => {
    setHeaderOpen(!headerOpen)
  }
  const closeSideBar = () => {
    setHeaderOpen(false)
  }
  const auth = useAuth() as AuthHook

  useEffect(() => {
    window.document.getElementsByTagName('HTML')[0].className = navOpen ? 'nav_open' : headerOpen ? 'topbar_open' : ''
  }, [headerOpen, navOpen])

  return (<>
    {auth.user !== null && <><Header toggleSideBar={toggleSideBar} toggleTopBar={toggleTopBar} navOpen={navOpen} headerOpen={headerOpen}/><SideNav toggleSideBar={closeSideBar}/></>}
    {children}
  </>)
}

export default WithHeaderAndSideNav

import React, { useEffect } from 'react'
import ReactDOM from 'react-dom'

const modalRoot = document.getElementById('modal-root')
export const Portal: React.FC = ({ children }) => {
  const el = document.createElement('div')
  useEffect(() => {
    if (modalRoot) {
      modalRoot.appendChild(el)
      return () => {
        modalRoot.removeChild(el)
      }
    }
  })
  return ReactDOM.createPortal(children, el)
}

export default Portal

import React from 'react'
import { AxiosFunctionReturnType } from '../../utils/asyncFunction'
import { useFetch } from '../../hooks/useFetch'
import { useRouter } from '../../hooks/useRouter'
import SmallSpinner from '../bootstrap/spinner/smallSpinners'

interface TopCardProps{
  className: string;
  title: string;
  getData: () => Promise<AxiosFunctionReturnType<number>>;
  icon: string;
  url: string;
}
const TopCard: React.FC<TopCardProps> = ({ className, title, getData, icon, url }) => {
  const { data, isLoading } = useFetch<number>({
    initialData: 0,
    reloadOn: [],
    fetchFunction: async () => {
      return await getData()
    }
  })
  const { push } = useRouter()
  const handleCardClick = () => {
    push(url)
  }
  return (
    <div className="col-md-3" style={{ cursor: 'pointer' }} onClick={handleCardClick}>
      <div className={`card card-stats card-${className}`}>
        <div className="card-body ">
          <div className="row">
            <div className="col-5">
              <div className="icon-big text-center">
                <i className={`la la-${icon}`}></i>
              </div>
            </div>
            <div className="col-7 d-flex align-items-center">
              <div className="numbers">
                <p className="card-category">{title}</p>
                <h4 className="card-title">{isLoading ? <SmallSpinner variant="white"/> : data}</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default TopCard

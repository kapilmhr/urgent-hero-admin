import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { HOME_ROUTE, LOGIN_ROUTE, LOGOUT_ROUTE, NOT_LOGGED_IN_ROUTE, UNAUTHORIZED_ROUTE } from './constants'
import Login from './login'
import Logout from './logout'
import PrivateRoutes from './privateRoutes/router'
import NotLoggedIn from '../components/notLoggedIn'
import Unauthorized from '../components/unauthorized'

const MainRoutes: React.FC = () => {
  return (
    <div className="wrapper">
      <Switch>
        <Route path={LOGIN_ROUTE} exact component={Login} />
        <Route path={LOGOUT_ROUTE} exact component={Logout} />
        <Route path={NOT_LOGGED_IN_ROUTE} exact component={NotLoggedIn} />
        <Route path={UNAUTHORIZED_ROUTE} exact component={Unauthorized} />
        <Route path={HOME_ROUTE} component={PrivateRoutes}/>
      </Switch>
    </div>
  )
}

export default MainRoutes

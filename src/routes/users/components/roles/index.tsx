import React from 'react'
import { rolesColorMap, rolesDisplayName } from '../../../../apis/users/constants'
import { Role } from '../../../../apis/users/index.d'

interface RolesProps{
  role: Role;
  className?: string;
}
const RolesComponent: React.FC<RolesProps> = ({ role, className }) => (
  <span className={`badge ${rolesColorMap[role]} ${className || ''}`}>{rolesDisplayName[role]}</span>
)

export default RolesComponent

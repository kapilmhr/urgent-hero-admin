import React, { useState } from 'react'
import { CRouteComponentProps } from '../../@types/route'
import Card from '../../components/template/card'
import { getUsers, getUsersCount } from '../../apis/users'
import Table, { TableHeader } from '../../components/template/table'
import Portal from '../../components/portal'
import Modal from '../../components/bootstrap/modal'
import { Container, Row, Column } from '../../components/bootstrap/layout'
import RolesComponent from './components/roles'
import { Form } from 'react-final-form'
import HTMLForm from '../../components/bootstrap/form'
import { convertToDefault } from '../../utils/date'
import MainPanel from '../../components/template/mainPanel'
import { FormRow, FormText } from '../../components/bootstrap/input'
import { User } from '../../apis/users/index.d'

const Dashboard: React.FC<CRouteComponentProps> = () => {
  const [modal, showModal] = useState<boolean>(false)
  const [currentUser, setCurrentUser] = useState<User|null>(null)
  const toggleModal = () => {
    showModal(!modal)
  }

  const onNameClick = (user: User) => {
    toggleModal()
    setCurrentUser(user)
  }
  const headers: TableHeader[] = [
    {
      name: 'Full Name',
      dataAttribute: 'displayName',
      formatter: (data: User) => {
        return (
          <a href={'#'} onClick={(e) => { e.preventDefault(); onNameClick(data) }}>{data.displayName || 'Unknown'}</a>
        )
      }
    },
    {
      name: 'Email',
      dataAttribute: 'email'
    },
    {
      name: 'Created',
      dataAttribute: 'metadata',
      formatter: (data: User) => {
        return convertToDefault(data.metadata.creationTime)
      }
    },
    {
      name: 'Last Signed in',
      dataAttribute: 'metadata',
      formatter: (data: User) => {
        return convertToDefault(data.metadata.lastSignInTime)
      }
    },
    {
      name: 'Roles',
      dataAttribute: 'customClaims',
      formatter: (data: User) => {
        return data.customClaims.roles.map((role, index) => (<RolesComponent role={role} key={`${role}-${index}`} className="mb-1"/>))
      }
    }
  ]
  const onSubmit = (values: User) => {
    console.log(values)
  }

  return (
    <MainPanel title="Users">
      <div className="row">
        <div className="col">
          <Card>
            <Table headers={headers} getData={getUsers} getCount={getUsersCount}/>
          </Card>
        </div>
      </div>
      {modal && currentUser &&
      <Portal>
        <Modal size={'lg'} show={modal} toggleModal={toggleModal} title={currentUser.displayName}>
          <Container>
            <Row>
              <Column className="col-3">
                <div className="u-img mb-3"><img className="full-width" src={currentUser.photoURL || './images/user.svg'} alt="user"/></div>
                {currentUser.customClaims.roles.map((role) => (<RolesComponent key={role} role={role} className="mb-1"/>))}
              </Column>
              <Column>
                <Form
                  onSubmit={onSubmit}
                  render={
                    ({ handleSubmit }) => {
                      return (
                        <HTMLForm onSubmit={handleSubmit}>
                          <FormRow>
                            <FormText value={currentUser.email} title={'Email'} className="col-md-6" helpText={`${currentUser.emailVerified === true ? '(Verified)' : '(Unverified)'}`}/>
                            <FormText value={currentUser.phoneNumber} title="Phone Number" className="col-md-6"/>
                          </FormRow>
                          <FormRow>
                            <FormText title={'Creation Time'} value={convertToDefault(currentUser.metadata.creationTime)} className={'col-md-6'}/>
                            <FormText title={'Last Signed In'} value={convertToDefault(currentUser.metadata.lastSignInTime)} className={'col-md-6'}/>
                          </FormRow>
                        </HTMLForm>
                      )
                    }
                  }
                />
              </Column>
            </Row>
          </Container>
        </Modal>
      </Portal>
      }
    </MainPanel>
  )
}

export default Dashboard

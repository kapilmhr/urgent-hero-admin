import { HeroStatusEnum } from '../../../../apis/heros/constants'

export const statusMap = {
  [HeroStatusEnum.REQUEST_CHANGES]: 'Changes Requested',
  [HeroStatusEnum.APPROVED]: 'Approved',
  [HeroStatusEnum.PENDING]: 'Pending',
  [HeroStatusEnum.DENIED]: 'Denied',
  '': ''
}

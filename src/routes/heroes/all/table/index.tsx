import React from 'react'
import { useFetch } from '../../../../hooks/useFetch'
import { Hero } from '../../../../apis/heros/types'
import CustomTable, { TableHeader } from '../../../../components/template/table'
import { getCategories } from '../../../../apis/jobCategories'
import { AxiosFunctionReturnType } from '../../../../utils/asyncFunction'
import { JobCategory } from '../../../../apis/jobCategories/index.d'
import { PaginationParams } from '../../../../apis/types'
import { Link } from 'react-router-dom'
import { HERO_DETAILS_ROUTE } from '../../../constants'

interface HeroTableProps{
  api: (params: PaginationParams) => Promise<AxiosFunctionReturnType<Hero[]>>;
  getCount: () => Promise<AxiosFunctionReturnType<number>>;
}
const HeroTable: React.FC<HeroTableProps> = ({ api, getCount }) => {
  const categories = useFetch<{[id: string]: JobCategory}>({
    initialData: {},
    reloadOn: [],
    fetchFunction: async () => {
      const [err, data] = await getCategories({})
      if (err) {
        return [err, null]
      }
      let normalizedData = {}
      if (data) {
        data.forEach((category) => {
          normalizedData = { ...normalizedData, [category.id]: category }
        })
      }
      return [err, normalizedData]
    }
  })

  const headers: TableHeader[] = [
    {
      name: 'Display Name',
      dataAttribute: 'displayName',
      formatter: (data: Hero) => {
        if (data) {
          return <Link to={`${HERO_DETAILS_ROUTE}/${data.id}?from=all`}>{data.displayName}</Link>
        }
        return '-'
      }
    },
    {
      name: 'Email',
      dataAttribute: 'email'
    },
    {
      name: 'Category',
      dataAttribute: 'categories',
      formatter: (data: Hero) => {
        if (data) {
          return data.categories && data.categories.map((category, index) => (<span className="badge badge-count" key={index}>{categories.data[category] && categories.data[category].title}</span>))
        }
        return '-'
      }
    }

  ]

  return (
    <CustomTable headers={headers} getData={api} getCount={getCount}/>
  )
}
export default HeroTable

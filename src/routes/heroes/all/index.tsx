import React from 'react'
import HeroTable from './table'
import { getHeroApplication, getHeroApplicationCount } from '../../../apis/heros'
import Card from '../../../components/template/card'

const AllHeros: React.FC = () => {
  return (
    <Card title="All" additionalClass="mt-3">
      <HeroTable api={getHeroApplication} getCount={getHeroApplicationCount}/>
    </Card>
  )
}
export default AllHeros

import React, { useState, useEffect } from 'react'
import { License, Qualification, Schedule } from '../../../../../apis/heros/types'
import { JobCategory } from '../../../../../apis/jobCategories/index.d'
import { Nav } from 'react-bootstrap'
import CategoryComponent from '../category'
import { NormalizedObject } from '../../../../../apis/types'
import { normalizeData } from '../../../../../utils/normalize'
interface CategoriesTabsProps{
  categories: string[];
  license: License[];
  qualification: Qualification[];
  schedule: Schedule[];
  allCategories: {[id: string]: JobCategory};
}
const CategoriesTabs: React.FC<CategoriesTabsProps> = ({ categories, license, qualification, schedule, allCategories }) => {
  const [currentTab, setCurrentTab] = useState<string>(categories[0] || '')
  const [normalizedLicence, setNormalizedLicence] = useState<NormalizedObject<License>>({})
  const [normalizedCertification, setNormalizedCertification] = useState<NormalizedObject<Qualification>>({})
  const [normalizedSchedule, setNormalizedSchedule] = useState<NormalizedObject<Schedule>>({})

  useEffect(() => {
    setNormalizedCertification(normalizeData<Qualification>(qualification, 'categoryId'))
    setNormalizedLicence(normalizeData<License>(license, 'categoryId'))
    setNormalizedSchedule(normalizeData<Schedule>(schedule, 'categoryId'))
  }, [qualification, license, schedule])
  return (
    <>
      <Nav variant="tabs" justify defaultActiveKey={currentTab}>
        {categories && categories.map((category) => {
          const catObj = allCategories[category] || {}
          return (
            <Nav.Item key={catObj.id}>
              <Nav.Link eventKey={catObj.id} onSelect={(eventKey: string) => setCurrentTab(eventKey)}>
                {catObj.title}
              </Nav.Link>
            </Nav.Item>
          )
        })}
      </Nav>
      <CategoryComponent key={currentTab} qualification={normalizedCertification} license={normalizedLicence} schedule={normalizedSchedule}
        categoryId={currentTab}/>
    </>
  )
}

export default CategoriesTabs

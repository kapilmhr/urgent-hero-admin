import React from 'react'
import Portal from '../../../../components/portal'
import { Container, Row, Column } from '../../../../components/bootstrap/layout'
import { Hero } from '../../../../apis/heros/types'
import { FormText } from '../../../../components/bootstrap/input'
import CategoriesTabs from './categoriesTabs'
import { JobCategory } from '../../../../apis/jobCategories/index.d'
import Modal from '../../../../components/bootstrap/modal'
interface HeroModalProps{
  currentUser: Hero;
  modal: boolean;
  toggleModal: () => void;
  categories: {[id: string]: JobCategory};
  scroll?: boolean;
  size?: 'sm' | 'lg' | 'xl';
  customClass?: string;
}
const HeroModal: React.FC<HeroModalProps> = ({ currentUser, modal, toggleModal, categories, scroll, size, customClass }) => {
  return (
    <>
      <Portal>
        <Modal size={size || 'lg'} show={modal} scrollable={scroll} toggleModal={toggleModal} title={currentUser.displayName} className={customClass}>
          <Container>
            <Row>
              <Column className="col-md-3">
                <div className="u-img mb-3"><img className="full-width" src={currentUser.photoURL || '/images/user.svg'} alt="user"/></div>
                <FormText value={currentUser.email} title={'Email'} className="" helpText={`${currentUser.emailVerified === true ? '(Verified)' : '(Unverified)'}`}/>
                <FormText value={currentUser.phoneNumber} title="Phone Number" className=""/>
              </Column>
              <Column>
                {currentUser.categories && currentUser.categories.length > 0 && Object.keys(categories).length > 0 &&
                  <CategoriesTabs categories={currentUser.categories || []} qualification={currentUser.qualification || []} license={currentUser.license || []} schedule={currentUser.schedule || []} allCategories={categories} />
                }
              </Column>
            </Row>

          </Container>
        </Modal>
      </Portal>
    </>
  )
}
export default HeroModal

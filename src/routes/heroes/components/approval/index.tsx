import React from 'react'
import { Form } from 'react-final-form'
import HTMLForm from '../../../../components/bootstrap/form'
import { Button } from 'react-bootstrap'
import { bindHeroToCategory, rejectHero, requestChanges } from '../../../../apis/hero-category-binders'
import { showSuccess } from '../../../../utils/notification'
import { FinalInput, FinalSelect } from '../../../../components/finalform/input'
import { ApplilcationStatus } from '../../constants'
import { Option, FormRow } from '../../../../components/bootstrap/input'
export interface ApprovalValues{
  remarks: string;
  status: ApplilcationStatus;
}

interface ApprovalProps{
  userId: string;
  email: string;
  categoryId: string;
  applicationId: string;
  initialValues?: ApprovalValues;
}
const Approval: React.FC<ApprovalProps> = ({ userId, email, categoryId, applicationId, initialValues }) => {
  const handleApprove = async (emaildescription: string) => {
    const [, data] = await bindHeroToCategory({ userId: [userId], categoryId, userEmail: [email], emaildescription, applicationId })
    if (data) {
      showSuccess('Hero Approved')
    }
  }
  const handleReject = async (emaildescription: string) => {
    const [err] = await rejectHero({ userId: [userId], categoryId, userEmail: [email], emaildescription, applicationId })
    if (!err) {
      showSuccess('Hero Rejected')
    }
  }

  const handleChangeRequest = async (emaildescription: string) => {
    const [err] = await requestChanges({ userId: [userId], categoryId, userEmail: [email], emaildescription, applicationId })
    if (!err) {
      showSuccess('Changes Requested')
    }
  }

  const onSubmit = async (values: ApprovalValues) => {
    switch (values.status) {
      case ApplilcationStatus.APPROVED:
        await handleApprove(values.remarks)
        break
      case ApplilcationStatus.CHANGES_REQUIRED:
        await handleChangeRequest(values.remarks)
        break
      case ApplilcationStatus.REJECTED:
        await handleReject(values.remarks)
        break
    }
    //
  }
  const options: Option[] = [
    { label: 'Approved', value: ApplilcationStatus.APPROVED },
    { label: 'Pending', value: ApplilcationStatus.PENDING },
    { label: 'Changes Required', value: ApplilcationStatus.CHANGES_REQUIRED },
    { label: 'Rejected', value: ApplilcationStatus.REJECTED }
  ]
  return (
    <Form
      onSubmit={onSubmit}
      initialValues={initialValues}
      render={(formProps) => {
        const { handleSubmit, pristine, submitting } = formProps
        return (
          <HTMLForm onSubmit={handleSubmit}>
            <FormRow>
              <FinalInput name="remarks" placeholder="Remarks" required formGroupClass="col-md-7"/>
              <FinalSelect options={options} name="status" type="password" required formGroupClass="col-md-3"/>
              <div className="col-md-2 approval-form-button"><Button disabled={pristine || submitting} variant="success" type="submit" >Submit</Button></div>
            </FormRow>
          </HTMLForm>
        )
      }}
    />
  )
}

export default Approval

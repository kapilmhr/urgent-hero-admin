import React from 'react'
import { User } from '../../../../apis/users/index.d'
import CustomTable, { TableHeader } from '../../../../components/template/table'
import { AxiosFunctionReturnType } from '../../../../utils/asyncFunction'
import { PaginationParams } from '../../../../apis/types'
import { Link } from 'react-router-dom'
import { HERO_DETAILS_ROUTE } from '../../../constants'

interface HeroTableProps{
  api: (params: PaginationParams) => Promise<AxiosFunctionReturnType<User[]>>;
}
const HeroTable: React.FC<HeroTableProps> = ({ api }) => {
  const headers: TableHeader[] = [
    {
      name: 'Display Name',
      dataAttribute: 'displayName',
      formatter: (data: User) => {
        if (data) {
          return <Link to={`${HERO_DETAILS_ROUTE}/${data.id}?from=approved`}>{data.displayName}</Link>
        }
        return '-'
      }
    },
    {
      name: 'Email',
      dataAttribute: 'email'
    }
  ]

  return (
    <CustomTable headers={headers} getData={api} getCount={async () => { return [null, 0] }} hidePagination={true}/>
  )
}
export default HeroTable

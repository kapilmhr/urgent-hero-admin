import React, { useState, useEffect } from 'react'
import { useFetch } from '../../../hooks/useFetch'
import { HeroAndCategory } from '../../../apis/hero-category-binders/types'
import { getBindedCategoryList, listofHeros } from '../../../apis/hero-category-binders'
import { Container, Row, Column } from '../../../components/bootstrap/layout'
import { JobCategory } from '../../../apis/jobCategories/index.d'
import { getCategories } from '../../../apis/jobCategories'
import { ButtonPrimary } from '../../../components/bootstrap/button'
import Spinner from '../../../components/bootstrap/spinner'
import HeroTable from './table'
import { Button } from 'react-bootstrap'
import Card from '../../../components/template/card'

const ApprovedHeros: React.FC = () => {
  const [binderId, setBinderHero] = useState<string>('')
  const [categoryId, setCategoryId] = useState<string>('')

  const [showTable, setShowTable] = useState<boolean>(false)
  const { data, isLoading } = useFetch<HeroAndCategory[]>({
    initialData: [],
    reloadOn: [],
    fetchFunction: async () => {
      return await getBindedCategoryList()
    }
  })
  useEffect(() => {
    if (binderId !== '') {
      setShowTable(true)
    }
  }, [binderId])
  const categories = useFetch<{[id: string]: JobCategory}>({
    initialData: {},
    reloadOn: [],
    fetchFunction: async () => {
      const [err, data] = await getCategories({ })
      if (err) {
        return [err, null]
      }
      let normalizedData = {}
      if (data) {
        data.forEach((category) => {
          normalizedData = { ...normalizedData, [category.id]: category }
        })
      }
      return [err, normalizedData]
    }
  })
  return (
    <>
      {showTable ? <>
        <Card title="Approved" additionalClass="mt-3">
          <h6><Button onClick={() => {
            setShowTable(false)
            setBinderHero('')
          }} variant="link">&larr;</Button>{categories.data[categoryId] && categories.data[categoryId].title}</h6>
          <HeroTable api={async () => {
            const [err, data] = await listofHeros(binderId)
            return [err, data ? data.heros : null]
          }}/>
        </Card>
      </> : <Container>
        <Row>
          {categories.data && data && data.map((heroAndCategory) => {
            const category = categories.data[heroAndCategory.categoryId]
            return (
              <Column key={heroAndCategory.id} className="col-sm-3 mt-3">
                <Card title={category && category.title}>
                  <p>Total: {heroAndCategory && heroAndCategory.userId && heroAndCategory.userId.length}</p>
                  <ButtonPrimary onClick={() => {
                    setBinderHero(heroAndCategory.id)
                    setCategoryId(heroAndCategory.categoryId)
                  }} className="float-right" title="View List"/>
                </Card>
              </Column>
            )
          })}
        </Row>
      </Container>}
      {data.length === 0 && !isLoading && 'No Data'}
      {isLoading && <Spinner/>}
    </>
  )
}
export default ApprovedHeros

import React from 'react'
import { Container, Row, Column } from '../../../components/bootstrap/layout'
import { FormText } from '../../../components/bootstrap/input'
import { useFetch } from '../../../hooks/useFetch'
import { Hero } from '../../../apis/heros/types'
import { useRouter } from '../../../hooks/useRouter'
import { getHeroById } from '../../../apis/heros'
import { getCategories } from '../../../apis/jobCategories'
import { JobCategory } from '../../../apis/jobCategories/index.d'
import MainPanel from '../../../components/template/mainPanel'
import FullPageLoading from '../../../components/fullPageLoading'
import { Link } from 'react-router-dom'
import { HEROES_ROUTE } from '../../constants'
import CategoriesTabs from './components/categoriesTabs'
import PaymentCard from './components/paymentCard'
import { getPaymentsOfUser } from '../../../apis/payments'

const initialData: Hero = {
  id: '',
  uid: '',
  email: '',
  displayName: '',
  photoURL: '',
  phoneNumber: '',
  emailVerified: false,
  disabled: false,
  metadata: {
    creationTime: '',
    lastSignInTime: ''
  },
  providerData: [],
  tokensValidAfterTime: '',
  passwordHash: '',
  passwordSalt: '',
  tenantId: '',
  customClaims: {
    roles: []
  }
}
const HeroDetails: React.FC = () => {
  const { query } = useRouter<{id: string; from: string}>()
  const categories = useFetch<{[id: string]: JobCategory}>({
    initialData: {},
    reloadOn: [],
    fetchFunction: async () => {
      const [err, data] = await getCategories({})
      if (err) {
        return [err, null]
      }
      let normalizedData: {[id: string]: JobCategory} = {}
      if (data) {
        data.forEach((category) => {
          normalizedData = { ...normalizedData, [category.id]: category }
        })
      }
      return [err, normalizedData]
    }
  })
  const { data, isLoading } = useFetch<Hero>({
    initialData,
    fetchFunction: async () => {
      return await getHeroById(query.id)
    },
    reloadOn: []
  })
  if (isLoading) {
    return <FullPageLoading/>
  }
  return (
    <MainPanel title={data.displayName}>
      <Link to={`${HEROES_ROUTE}/${query.from}`}>&larr;</Link>
      <Container>
        <Row>
          <Column className="col-md-3">
            <div className="u-img mb-3"><img className="full-width" src={data.photoURL || '/images/user.svg'} alt="user"/></div>
            <FormText value={data.email} title={'Email'} className="" helpText={`${data.emailVerified === true ? '(Verified)' : '(Unverified)'}`}/>
            <FormText value={data.phoneNumber} title="Phone Number" className=""/>
          </Column>
          <Column>
            <PaymentCard title="Earning since start" getData={(id) => getPaymentsOfUser({ id })} id={query.id}/>
            {data.categories && data.categories.length > 0 && Object.keys(categories).length > 0 &&
          <CategoriesTabs categories={data.categories || []} qualification={data.qualification || []} license={data.license || []} schedule={data.schedule || []} allCategories={categories.data} />
            }
          </Column>
        </Row>

      </Container>
    </MainPanel>
  )
}

export default HeroDetails

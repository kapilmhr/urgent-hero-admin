import React, { useState } from 'react'
import { License, Qualification, Schedule } from '../../../../../apis/heros/types'
import { NormalizedObject } from '../../../../../apis/types'
import { Row, Column } from '../../../../../components/bootstrap/layout'
import PaymentCard from '../paymentCard'
import { getPaymentsOfUser } from '../../../../../apis/payments'
import { useRouter } from '../../../../../hooks/useRouter'

interface CategoryProp{
  qualification: NormalizedObject<Qualification>;
  license: NormalizedObject<License>;
  schedule: NormalizedObject<Schedule>;
  categoryId: string;
}
const daysMap: any = {
  0: 'Sunday',
  1: 'Monday',
  2: 'Tuesday',
  3: 'Wednesday',
  4: 'Thursday',
  5: 'Friday',
  6: 'Saturday'
}
const CategoryComponent: React.FC<CategoryProp> = ({ qualification, license, schedule, categoryId }) => {
  const singleQualification = qualification[categoryId]
  const singleLicense = license[categoryId]
  const singleSchedule = schedule[categoryId]
  const { query } = useRouter<{id: string}>()
  return (
    <div className="mt-4 ">
      <PaymentCard title="Earning since start" getData={() => getPaymentsOfUser({ categoryId, id: query.id })} id={''}/>
      {singleSchedule ? <>     <Row>
        <Column>
          <br/>
          <label>Schedule</label>
        </Column>
      </Row>
      <Row>
        <Column>
          <small>Charges Breakdown</small><br/>
        </Column>
      </Row>
      <Row>
        <Column>
          <small>Fixed Cost:</small><br/>
          {singleSchedule && singleSchedule.chargesBreakUp.fixedCost}
        </Column>
        <Column>
          <small>Hour Cost:</small><br/>
          {singleSchedule && singleSchedule.chargesBreakUp.extraHour}
        </Column>
      </Row>
      <Row>
        <Column>
          <br/>
          <small>Working Hour</small><br/>
        </Column>
      </Row>
      <Row>
        <Column>
          <small>Days:</small><br/>
          {singleSchedule && singleSchedule.workingSchedule.days.map((day) => (<span className="badge badge-primary" key={day}>{daysMap[day]}</span>))}
        </Column>
        <Column>
          <small>Start Time:</small><br/>
          {singleSchedule && singleSchedule.workingSchedule.startTime}
        </Column>
        <Column>
          <small>End Time:</small><br/>
          {singleSchedule && singleSchedule.workingSchedule.endTime}
        </Column>
      </Row></> : <div>No Data</div> }
      {singleLicense ? <><Row>
        <Column>
          <br/>
          <label>License</label>
        </Column>
      </Row>
      <Row>
        <Column>
          {singleLicense && singleLicense.licenseNumber}
        </Column>
      </Row>
      <br/>
      <Row>
        <Column>
          <label>Front:</label>
          {singleLicense && singleLicense.imageFront && <img className="full-width" src={singleLicense.imageFront}/>}
        </Column>
        <Column>
          <label>Back:</label>
          {singleLicense && singleLicense.imageFront && <img className="full-width" src={singleLicense.imageBack}/>}
        </Column>
      </Row></> : <div>No Data</div> }

      {singleQualification ? <>
        <Row>
          <Column>
            <br/>
            <label>Qualification</label>
          </Column>
        </Row>
        <Row>
          <Column>
            {singleQualification && singleQualification.title}
          </Column>
        </Row>
        <br/>
        <Row>
          <Column>
            {singleQualification && singleQualification.image && <img className="half-width" src={singleQualification.image}/>}
          </Column>
        </Row></> : <div>No Data</div> }
      <br/>
    </div>
  )
}
export default CategoryComponent

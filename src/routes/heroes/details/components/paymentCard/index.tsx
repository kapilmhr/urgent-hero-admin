import React, { useEffect, useState } from 'react'
import Card from '../../../../../components/template/card'
import { AxiosFunctionReturnType } from '../../../../../utils/asyncFunction'
import { Payment, TotalPayment } from '../../../../../apis/payments/types'
import { useFetch } from '../../../../../hooks/useFetch'
interface PaymentCardProps{
  title: string;
  getData: (id: string) => Promise<AxiosFunctionReturnType<TotalPayment>>;
  id: string;
}
const PaymentCard: React.FC<PaymentCardProps> = ({ title, getData, id }) => {
  const { data } = useFetch<TotalPayment>({
    initialData: {
      payments: [],
      total: 0,
      count: 0
    },
    fetchFunction: async () => {
      return await getData(id)
    },
    reloadOn: [id]
  })
  return (
    <div className="card card-stats">
      <div className="card-body">
        <p className="fw-bold mt-1">{title}</p>
        <div className="row">
          <div className="col-5">
            <div className="icon-big text-center icon-primary">
              <i className="la la-money text-primary"></i>
            </div>
          </div>
          <div className="col-7 d-flex align-items-center">
            <div className="numbers">
              <p className="card-category">$</p>
              <h4 className="card-title">{data.total}</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default PaymentCard

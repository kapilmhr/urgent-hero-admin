import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { APPROVED_HEROES, HERO_APPLICATIONS } from './constants'
import Spinner from '../../components/bootstrap/spinner'
import { HEROES_ROUTE } from '../constants'
import { NavTab, Tab } from '../../components/bootstrap/navTab'
import MainPanel from '../../components/template/mainPanel'
import { Column } from '../../components/bootstrap/layout'

const ApprovedHeroes = React.lazy(() => import('./approved'))
const AllHeros = React.lazy(() => import('./all'))
const Applications = React.lazy(() => import('./applications'))
const HeroRouter: React.FC = () => {
  const tabs: Tab[] = [
    {
      title: 'Applications',
      url: HERO_APPLICATIONS
    },
    {
      title: 'All',
      url: HEROES_ROUTE
    },
    {
      title: 'Approved',
      url: APPROVED_HEROES
    }
  ]
  return (
    <MainPanel title="Heroes">
      <div className="row">

        <Column className="col-12">
          <NavTab tabs={tabs}/>
        </Column>
        <Column>
          <React.Suspense fallback={<Spinner/>}>
            <Switch>
              <Route path={`${HEROES_ROUTE}/all`} exact render={() => <Redirect to={HEROES_ROUTE}/>}/>
              <Route path={HEROES_ROUTE} exact component={AllHeros}/>
              <Route path={APPROVED_HEROES} exact component={ApprovedHeroes}/>
              <Route path={HERO_APPLICATIONS} exact component={Applications}/>
            </Switch>
          </React.Suspense>
        </Column>
      </div>
    </MainPanel>
  )
}

export default HeroRouter

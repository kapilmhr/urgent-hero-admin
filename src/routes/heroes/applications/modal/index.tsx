import React from 'react'
import Portal from '../../../../components/portal'
import { Container, Row, Column } from '../../../../components/bootstrap/layout'
import { FormText } from '../../../../components/bootstrap/input'
import { JobCategory } from '../../../../apis/jobCategories/index.d'
import Modal from '../../../../components/bootstrap/modal'
import CategoriesTabs from '../../components/modal/categoriesTabs'
import { HeroApplication } from '../../../../apis/applications/types'
import Approval from '../../components/approval'
interface HeroModalProps{
  application: HeroApplication;
  modal: boolean;
  toggleModal: () => void;
  categories: {[id: string]: JobCategory};
  scroll?: boolean;
  size?: 'sm' | 'lg' | 'xl';
  customClass?: string;
}
const ApplicationModal: React.FC<HeroModalProps> = ({ application, modal, toggleModal, categories, scroll, size, customClass }) => {
  return (
    <>
      <Portal>
        <Modal size={size || 'lg'} show={modal} scrollable={scroll} toggleModal={toggleModal} title={application.hero.displayName} className={customClass}>
          <Container>
            <Row className="row-eq-height">
              <Column className="col-md-3">
                <div className="u-img mb-3"><img className="full-width" src={application.hero.photoURL || '/images/user.svg'} alt="user"/></div>
                <FormText value={application.hero.email} title={'Email'} className="" helpText={`${application.hero.emailVerified === true ? '(Verified)' : '(Unverified)'}`}/>
                <FormText value={application.hero.phoneNumber} title="Phone Number" className=""/>
              </Column>
              <Column>
                {application.categoryId && application.categoryId.length > 0 && Object.keys(categories).length > 0 &&
                  <CategoriesTabs categories={application.categoryId || []} qualification={application.hero.qualification || []} license={application.hero.license || []} schedule={application.hero.schedule || []} allCategories={categories} />
                }
              </Column>
            </Row>
            <hr/>
            <Row>
              <Column className="col-md-12">
                <Approval initialValues={{ remarks: application.remarks, status: application.status }} userId={application.hero.id} email={application.hero.email} categoryId={application.categoryId[0]} applicationId={application.id}/>
              </Column>
            </Row>
          </Container>
        </Modal>
      </Portal>
    </>
  )
}
export default ApplicationModal

import { ApplilcationStatus } from '../../constants'

export const statusMap = {
  [ApplilcationStatus.CHANGES_REQUIRED]: 'Changes Requested',
  [ApplilcationStatus.APPROVED]: 'Approved',
  [ApplilcationStatus.PENDING]: 'Pending',
  [ApplilcationStatus.REJECTED]: 'Rejected'
}

export const applicationStatusClassMap = {
  [ApplilcationStatus.CHANGES_REQUIRED]: 'warning',
  [ApplilcationStatus.APPROVED]: 'success',
  [ApplilcationStatus.PENDING]: 'primary',
  [ApplilcationStatus.REJECTED]: 'danger'
}

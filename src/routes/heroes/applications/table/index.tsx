import React, { useState } from 'react'
import { useFetch } from '../../../../hooks/useFetch'
import { HeroApplication } from '../../../../apis/applications/types'
import CustomTable, { TableHeader } from '../../../../components/template/table'
import { getCategories } from '../../../../apis/jobCategories'
import { statusMap, applicationStatusClassMap } from './constants'
import { AxiosFunctionReturnType } from '../../../../utils/asyncFunction'
import { JobCategory } from '../../../../apis/jobCategories/index.d'
import { PaginationParams } from '../../../../apis/types'
import { ApplilcationStatus } from '../../constants'
import ApplicationModal from '../modal'

interface HeroTableProps{
  api: (params: PaginationParams) => Promise<AxiosFunctionReturnType<HeroApplication[]>>;
  getCount: () => Promise<AxiosFunctionReturnType<number>>;
}
const HeroTable: React.FC<HeroTableProps> = ({ api, getCount }) => {
  const [modal, setModal] = useState<boolean>(false)
  const [application, setCurrentUser] = useState<HeroApplication|null>(null)
  const toggleModal = () => {
    setModal(!modal)
  }
  const categories = useFetch<{[id: string]: JobCategory}>({
    initialData: {},
    reloadOn: [],
    fetchFunction: async () => {
      const [err, data] = await getCategories({})
      if (err) {
        return [err, null]
      }
      let normalizedData = {}
      if (data) {
        data.forEach((category) => {
          normalizedData = { ...normalizedData, [category.id]: category }
        })
      }
      return [err, normalizedData]
    }
  })
  const viewUser = (user: HeroApplication) => {
    setCurrentUser(user)
    toggleModal()
  }

  const headers: TableHeader[] = [
    {
      name: 'Display Name',
      dataAttribute: 'hero',
      formatter: (data: HeroApplication) => {
        if (data.hero) {
          return <a onClick={(e) => { e.preventDefault(); viewUser(data) }} href="#">{data.hero.displayName}</a>
        }
        return '-'
      }
    },
    {
      name: 'Email',
      dataAttribute: 'hero',
      formatter: (data: HeroApplication) => {
        if (data.hero) {
          return data.hero.email
        }
        return '-'
      }
    },
    {
      name: 'Category',
      dataAttribute: 'categoryId',
      formatter: (data: HeroApplication) => {
        if (data) {
          return data.categoryId && data.categoryId.map((category, index) => (<span className="badge badge-count" key={index}>{categories.data[category] && categories.data[category].title}</span>))
        }
        return '-'
      }
    },
    {
      name: 'Status',
      dataAttribute: 'hero',
      formatter: (data: HeroApplication) => {
        return <span className={`badge badge-${applicationStatusClassMap[data.status || ApplilcationStatus.PENDING]}`}>{statusMap[data.status || ApplilcationStatus.PENDING]}</span>
      }
    }
  ]

  return (
    <>
      <CustomTable headers={headers} getData={api} getCount={getCount}/>
      {modal && application &&
        <ApplicationModal customClass="application" modal={modal} size="xl" scroll={true} application={application} categories={categories.data} toggleModal={toggleModal} />
      }
    </>
  )
}
export default HeroTable

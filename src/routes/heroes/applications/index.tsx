import React from 'react'
import HeroTable from './table'
import Card from '../../../components/template/card'
import { getApplications, getApplicationCount } from '../../../apis/applications'

const Applications: React.FC = () => {
  return (
    <Card title="Applications" additionalClass="mt-3">
      <HeroTable api={getApplications} getCount={getApplicationCount}/>
    </Card>
  )
}
export default Applications

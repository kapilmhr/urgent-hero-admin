import { HEROES_ROUTE } from '../constants'

export const APPROVED_HEROES = `${HEROES_ROUTE}/approved`
export const OTHER_HEROES = `${HEROES_ROUTE}`
export const HERO_APPLICATIONS = `${HEROES_ROUTE}/applications`

export enum ApplilcationStatus {
  APPROVED = 'APPROVED',
  REJECTED = 'REJECTED',
  PENDING = 'PENDING',
  CHANGES_REQUIRED = 'CHANGES_REQUIRED'
}

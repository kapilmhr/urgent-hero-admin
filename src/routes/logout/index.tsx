import React, { useEffect } from 'react'
import { useAuth, AuthHook } from '../../hooks/useAuth/useAuth'
import { useRouter } from '../../hooks/useRouter'
import { LOGIN_ROUTE } from '../constants'
import Spinner from '../../components/bootstrap/spinner'

const Logout: React.FC = () => {
  const auth = useAuth() as AuthHook
  const router = useRouter()
  useEffect(() => {
    const signout = async () => {
      await auth.signout()
      router.push(LOGIN_ROUTE)
    }
    signout()
  }, [])
  return (
    <div className="full-page-container">
      <div style={{ margin: '0 auto' }}><Spinner/></div>

    </div>
  )
}
export default Logout

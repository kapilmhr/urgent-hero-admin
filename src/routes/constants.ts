import { URGENT_ADMIN, URGENT_UNBOARDING, URGENT_SUPER_ADMIN } from '../apis/users/constants'
export const NOT_LOGGED_IN_ROUTE: '/welcome' = '/welcome'
export const DASHBOARD_ROUTE: '/dashboard' = '/dashboard'
export const USERS_ROUTE: '/users' = '/users'
export const LOGIN_ROUTE: '/login' = '/login'
export const LOGOUT_ROUTE: '/logout' = '/logout'
export const HOME_ROUTE: '/' = '/'
export const CATEGORIES_ROUTE: '/categories' = '/categories'
export const ROLES_ROUTE: '/roles' = '/roles'
export const UNAUTHORIZED_ROUTE: '/unauthorized' = '/unauthorized'
export const HEROES_ROUTE: '/heroes' = '/heroes'
export const HERO_DETAILS_ROUTE: '/hero-details' = '/hero-details'
export const roleRouteMap = {
  [URGENT_UNBOARDING]: DASHBOARD_ROUTE,
  [URGENT_ADMIN]: CATEGORIES_ROUTE,
  [URGENT_SUPER_ADMIN]: ROLES_ROUTE
}

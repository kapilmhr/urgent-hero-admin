import React, { useState } from 'react'
import Table, { TableHeader } from '../../../components/template/table'
import Portal from '../../../components/portal'
import Modal from '../../../components/bootstrap/modal'
import { Container, Row, Column } from '../../../components/bootstrap/layout'
import { Form } from 'react-final-form'
import HTMLForm from '../../../components/bootstrap/form'
import { convertToDefault } from '../../../utils/date'
import MainPanel from '../../../components/template/mainPanel'
import { FormRow, FormText } from '../../../components/bootstrap/input'
import { Emergency } from '../../../apis/emergencies/types'
import { getEmergencies, getEmergencyCount } from '../../../apis/emergencies'

const EmergencyComponent: React.FC = () => {
  const [modal, showModal] = useState<boolean>(false)
  const [currentUser, setCurrentUser] = useState<Emergency|null>(null)
  const toggleModal = () => {
    showModal(!modal)
  }

  const onNameClick = (user: Emergency) => {
    toggleModal()
    setCurrentUser(user)
  }
  const headers: TableHeader[] = [
    {
      name: 'User',
      dataAttribute: 'user',
      formatter: (data: Emergency) => {
        if (data.user) {
          return (
            <a href={'#'} onClick={(e) => { e.preventDefault(); onNameClick(data) }}>{data.user.displayName || 'Unknown User'}</a>
          )
        }
        return '-'
      }
    },
    {
      name: 'Assigned Hero',
      dataAttribute: 'hero',
      formatter: (data: Emergency) => {
        if (data.hero) {
          return (
            <a href={'#'} onClick={(e) => { e.preventDefault(); onNameClick(data) }}>{data.hero.displayName || 'Unknown Hero'}</a>
          )
        }
        return 'Unassigned'
      }
    },
    {
      name: 'Category Name',
      dataAttribute: 'category',
      formatter: (data: Emergency) => {
        if (data.category) {
          return <span className="badge badge-count">{data.category.title}</span>
        }
        return '-'
      }
    },
    {
      name: 'Description',
      dataAttribute: 'description'
    },
    {
      name: 'Status',
      dataAttribute: 'status'
    }
  ]
  const onSubmit = (values: Emergency) => {
    console.log(values)
  }

  return (
    <>
      <Table headers={headers} getData={getEmergencies} getCount={getEmergencyCount}/>
      {/* {modal && currentUser &&
      <Portal>
        <Modal size={'lg'} show={modal} toggleModal={toggleModal} title={currentUser.displayName}>
          <Container>
            <Row>
              <Column className="col-3">
                <div className="u-img mb-3"><img className="full-width" src={currentUser.photoURL || './images/user.svg'} alt="user"/></div>
                {currentUser.customClaims.roles.map((role) => (<RolesComponent key={role} role={role} className="mb-1"/>))}
              </Column>
              <Column>
                <Form
                  onSubmit={onSubmit}
                  render={
                    ({ handleSubmit }) => {
                      return (
                        <HTMLForm onSubmit={handleSubmit}>
                          <FormRow>
                            <FormText value={currentUser.email} title={'Email'} className="col-md-6" helpText={`${currentUser.emailVerified === true ? '(Verified)' : '(Unverified)'}`}/>
                            <FormText value={currentUser.phoneNumber} title="Phone Number" className="col-md-6"/>
                          </FormRow>
                          <FormRow>
                            <FormText title={'Creation Time'} value={convertToDefault(currentUser.metadata.creationTime)} className={'col-md-6'}/>
                            <FormText title={'Last Signed In'} value={convertToDefault(currentUser.metadata.lastSignInTime)} className={'col-md-6'}/>
                          </FormRow>
                        </HTMLForm>
                      )
                    }
                  }
                />
              </Column>
            </Row>
          </Container>
        </Modal>
      </Portal>
      } */}
    </>
  )
}

export default EmergencyComponent

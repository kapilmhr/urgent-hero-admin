import React from 'react'
import MainPanel from '../../components/template/mainPanel'
import TopCard from '../../components/topCards'
import { getUsersCount } from '../../apis/users'
import { getHeroApplicationCount } from '../../apis/heros'
import { USERS_ROUTE, HEROES_ROUTE, DASHBOARD_ROUTE } from '../constants'
import Card from '../../components/template/card'
import EmergencyComponent from './emergencies'
import { getApplicationCount } from '../../apis/applications'
import { getAllPayments, getAllPaymentsForDashboard } from '../../apis/payments'
import PieChart from '../../components/chartjs/piechart'
import EmergencySummaryChart from './components/emergencyChart'
import { getAllEmergenciesForDashboard } from '../../apis/emergencies'
import BarChart from '../../components/chartjs/barchart'

const Dashboard: React.FC = () => {
  return (
    <MainPanel title="Dashboard">
      <div className="row">
        <TopCard className="warning" getData={getUsersCount} title="Users" icon="users" url={USERS_ROUTE}/>
        <TopCard className="success" getData={getHeroApplicationCount} title="All Heroes" icon="file-text" url={HEROES_ROUTE}/>
        <TopCard className="danger" getData={async () => {
          const [err, data] = await getAllPayments()
          if (data) {
            return [err, data.total]
          }
          return [err, data]
        }} title="Total Earnings" icon="money" url={DASHBOARD_ROUTE}/>
        <TopCard className="primary" getData={getApplicationCount} title="Hero Applications" icon="user" url={`${HEROES_ROUTE}/applications`}/>
      </div>
      <div className="row">
        <div className="col-sm-6">
          <Card title="Earning Chart" subtitle="Top Earnings per category">
            <PieChart id="categories" getData={getAllPaymentsForDashboard}/>
          </Card>
        </div>
        <div className="col-sm-6">
          <Card title="Hero Requests" subtitle="Request per category">
            <BarChart id="categories" getData={getAllEmergenciesForDashboard}/>
          </Card>
        </div>
      </div>
      <EmergencySummaryChart/>
      <div className="row">
        <div className="col-sm-12">
          <Card title="Total Emergencies" subtitle="What are we doing most of the time.">
            <EmergencyComponent/>
          </Card>
        </div>

      </div>
    </MainPanel>
  )
}

export default Dashboard

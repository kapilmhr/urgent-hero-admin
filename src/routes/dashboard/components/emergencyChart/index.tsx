import React from 'react'
import { useFetch } from '../../../../hooks/useFetch'
import { getEmergencyCount, getFailedEmergencyCount } from '../../../../apis/emergencies'

const EmergencySummaryChart: React.FC = () => {
  const { data } = useFetch<number>({
    initialData: 0,
    fetchFunction: async () => {
      return await getEmergencyCount()
    },
    reloadOn: []
  })
  const pending = useFetch<number>({
    initialData: 0,
    fetchFunction: async () => {
      return await getEmergencyCount('PENDING')
    },
    reloadOn: []
  })
  const getPendingPercentatge = () => (pending.data && data ? ((pending.data / data) * 100).toPrecision(3) : '-')

  const canceled = useFetch<number>({
    initialData: 0,
    fetchFunction: async () => {
      return await getEmergencyCount('CANCELED')
    },
    reloadOn: []
  })
  const getCanceledPercentatge = () => (pending.data && data ? ((canceled.data / data) * 100).toPrecision(3) : '-')

  const others = useFetch<number>({
    initialData: 0,
    fetchFunction: async () => {
      return await getFailedEmergencyCount()
    },
    reloadOn: []
  })
  const getOthersEmergencies = () => (pending.data && data ? ((others.data / data) * 100).toPrecision(3) : '-')

  const inProgress = useFetch<number>({
    initialData: 0,
    fetchFunction: async () => {
      return await getEmergencyCount('IN_PROGRESS')
    },
    reloadOn: []
  })
  const getInProgressEmergency = () => (pending.data && data ? ((inProgress.data / data) * 100).toPrecision(3) : '-')

  const resolved = useFetch<number>({
    initialData: 0,
    fetchFunction: async () => {
      return await getEmergencyCount('COMPLETED')
    },
    reloadOn: []
  })
  const getResolvedEmergencies = () => (pending.data && data ? ((resolved.data / data) * 100).toPrecision(3) : '-')
  return (
    <div className="row row-card-no-pd">
      <div className="col-md-3">
        <div className="card">
          <div className="card-body">
            <p className="fw-bold mt-1">Total Emergencies</p>
            <h4><b>{data && data}</b></h4>
            {/* <a href="#" className="btn btn-primary btn-full text-left mt-3 mb-3"><i className="la la-plus"></i> Add Balance</a> */}
          </div>
          <div className="card-footer">
            <ul className="nav">
              <li className="nav-item"><a className="btn btn-default btn-link" href="#"><i className="la la-history"></i> History</a></li>
              <li className="nav-item ml-auto"><a className="btn btn-default btn-link" href="#"><i className="la la-refresh"></i> Refresh</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div className="col-md-9">
        <div className="card">
          <div className="card-body">
            <div className="progress-card">
              <div className="d-flex justify-content-between mb-1">
                <span className="text-muted">Resolved</span>
                <span className="text-muted fw-bold"> {getResolvedEmergencies()}%</span>
              </div>
              <div className="progress mb-2" style={{ height: '7px' }}>
                <div className="progress-bar bg-info" role="progressbar" style={{ width: `${getResolvedEmergencies()}%` }}></div>
              </div>
            </div>
            <div className="progress-card">
              <div className="d-flex justify-content-between mb-1">
                <span className="text-muted">Pending</span>
                <span className="text-muted fw-bold"> {getPendingPercentatge()}%</span>
              </div>
              <div className="progress mb-2" style={{ height: '7px' }}>
                <div className="progress-bar bg-info" role="progressbar" style={{ width: `${getPendingPercentatge()}%` }}></div>
              </div>
            </div>
            <div className="progress-card">
              <div className="d-flex justify-content-between mb-1">
                <span className="text-muted">In Progress</span>
                <span className="text-muted fw-bold"> {getInProgressEmergency()}%</span>
              </div>
              <div className="progress mb-2" style={{ height: '7px' }}>
                <div className="progress-bar bg-info" role="progressbar" style={{ width: `${getInProgressEmergency()}%` }}></div>
              </div>
            </div>
            <div className="progress-card">
              <div className="d-flex justify-content-between mb-1">
                <span className="text-muted">Canceled</span>
                <span className="text-muted fw-bold"> {getCanceledPercentatge()}%</span>
              </div>
              <div className="progress mb-2" style={{ height: '7px' }}>
                <div className="progress-bar bg-danger" role="progressbar" style={{ width: `${getCanceledPercentatge()}%` }}></div>
              </div>
            </div>
            <div className="progress-card">
              <div className="d-flex justify-content-between mb-1">
                <span className="text-muted">Other</span>
                <span className="text-muted fw-bold"> {getOthersEmergencies()}%</span>
              </div>
              <div className="progress mb-2" style={{ height: '7px' }}>
                <div className="progress-bar bg-warning" role="progressbar" style={{ width: `${getOthersEmergencies()}%` }}></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <div className="col-md-3">
      <PaymentCard title="Total Earning" getData={() => getAllPayments()} id={''}/>
    </div> */}
    </div>
  )
}

export default EmergencySummaryChart

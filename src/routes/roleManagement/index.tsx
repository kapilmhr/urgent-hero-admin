import React, { useState } from 'react'
import MainPanel from '../../components/template/mainPanel'
import Card from '../../components/template/card'
import CustomTable, { TableHeader } from '../../components/template/table'
import { User } from '../../apis/users/index.d'
import RolesComponent from '../users/components/roles'
import { getAdmins, assignRoles } from '../../apis/users'
import AssignRoles, { SubmitRoles } from './components/assignRoles'

const RoleManagement: React.FC = () => {
  const [refresh, setRefresh] = useState<boolean>(false)
  const headers: TableHeader[] = [
    {
      name: 'Full Name',
      dataAttribute: 'displayName'
    },
    {
      name: 'Email',
      dataAttribute: 'email'
    },
    {
      name: 'Roles',
      dataAttribute: 'customClaims',
      formatter: (data: User) => {
        return data.customClaims.roles.map((role, index) => (<RolesComponent role={role} key={`${role}-${index}`} className="mb-1"/>))
      }
    }
  ]
  const handleSubmit = async (values: SubmitRoles) => {
    const data = await assignRoles(values.userId, values.roles)
    setRefresh(!refresh)
    return data
  }
  return (
    <>
      <MainPanel title="Role Management">
        <div className="row">
          <div className="col-sm-8">
            <Card title="Admin Users">
              <CustomTable key={`${refresh}`} headers={headers} getData={getAdmins} hidePagination={true} getCount={async () => {
                return [null, 0]
              }}/>
            </Card>
          </div>
          <div className="col-sm-4">
            <AssignRoles submit={handleSubmit}/>
          </div>
        </div>
      </MainPanel>
    </>
  )
}

export default RoleManagement

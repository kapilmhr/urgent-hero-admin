import React, { useState, useEffect, useReducer } from 'react'
import HTMLForm from '../../../../components/bootstrap/form'
import { ButtonPrimary } from '../../../../components/bootstrap/button'
import AsyncSelect from 'react-select/async'
import { getUsersByEmail } from '../../../../apis/users'
import { User, Role, RolesForSelect } from '../../../../apis/users/index.d'
import Select, { ValueType, components } from 'react-select'
import Card from '../../../../components/template/card'
import { rolesListForSelect } from '../../../../apis/users/constants'
import { FormGroupBase } from '../../../../components/bootstrap/input'
import { AxiosFunctionReturnType } from '../../../../utils/asyncFunction'
import { showSuccess } from '../../../../utils/notification'

export interface SubmitRoles{
  userId: string;
  roles: Role[];
}
interface CreateCategoriesProps{
  submit: (values: SubmitRoles) => Promise<AxiosFunctionReturnType<null>>;
}
const AssignRoles: React.FC<CreateCategoriesProps> = ({ submit }) => {
  const [user, setUser] = useState<User|null>(null)
  const [roles, setRoles] = useState<RolesForSelect[]>([])
  const [submitting, setSubmitting] = useState<boolean>(false)
  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const rolesKey: Role[] = roles.map((role) => role.value)
    if (roles.length && user) {
      setSubmitting(true)
      const [err] = await submit({ userId: user.id, roles: rolesKey })
      if (!err) {
        showSuccess('Success')
        setUser(null)
        setRoles([])
      }
      setSubmitting(false)
    }
  }
  const handleSearch = async (email: string) => {
    const [, data] = await getUsersByEmail({ email })
    if (data) {
      return data
    }
    return []
  }

  const getLabel = (option: User) => {
    return option.email
  }
  const getValue = (option: User) => {
    return option.id
  }
  const handleChange = (val: ValueType<User>) => {
    setUser(val as User)
  }
  const isOptionSelected = (option: User) => {
    return user ? user.id === option.id : false
  }
  const Option = (props: any) => {
    return (
      <components.Option {...props}>
        <div>{props.data && props.data.displayName}</div>
        <div>{props.data && props.data.email}</div>
      </components.Option>
    )
  }
  const handleRoleChange = (value: ValueType<RolesForSelect[]>) => {
    setRoles(value as RolesForSelect[])
  }

  return (
    <Card title="Assign roles">
      <HTMLForm onSubmit={onSubmit}>
        <FormGroupBase title="Email">
          <AsyncSelect value={user} isClearable components={{ Option }} onChange={handleChange} getOptionLabel={getLabel} getOptionValue={getValue} loadOptions={handleSearch} isOptionSelected={isOptionSelected}/>
        </FormGroupBase>
        <FormGroupBase title="Roles">
          <Select value={roles} options={rolesListForSelect as any} isMulti onChange={handleRoleChange} />
        </FormGroupBase>
        {/* <FinalInput name="roles" title="Roles" type="textarea"/> */}
        <ButtonPrimary title="Assign" type="submit" className="float-right" disabled={!user || submitting}/>
      </HTMLForm>
    </Card>
  )
}

export default AssignRoles

import React from 'react'
import Portal from '../../../../components/portal'
import Modal from '../../../../components/bootstrap/modal'
import { Form, FormRenderProps } from 'react-final-form'
import { JobCategory } from '../../../../apis/jobCategories/index.d'
import HTMLForm from '../../../../components/bootstrap/form'
import { FinalInput } from '../../../../components/finalform/input'
import { ButtonPrimary } from '../../../../components/bootstrap/button'
interface CreateCategoriesProps{
  show: boolean;
  toggleModal: () => void;
  submit: (values: JobCategory) => Promise<void>;
}
const CreateCategories: React.FC<CreateCategoriesProps> = ({ show, toggleModal, submit }) => {
  const onSubmit = async (values: JobCategory) => {
    await submit(values)
    toggleModal()
  }
  return (
    <Portal>
      <Modal show={show} toggleModal={toggleModal} title="Create Categories" size="lg">
        <Form
          onSubmit={onSubmit}
          render={({ handleSubmit, pristine, submitting }: FormRenderProps<JobCategory>) => {
            return (
              <HTMLForm onSubmit={handleSubmit}>
                <FinalInput name="title" title="Title"/>
                <FinalInput name="description" title="Desription" type="textarea"/>
                <ButtonPrimary title="Create" type="submit" className="float-right" disabled={pristine || submitting}/>
              </HTMLForm>
            )
          }}
        />
      </Modal>
    </Portal>
  )
}

export default CreateCategories

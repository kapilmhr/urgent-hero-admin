import React, { useState } from 'react'
import { getCategories, createCategories, getCategoriesCount } from '../../apis/jobCategories'
import { JobCategory } from '../../apis/jobCategories/index.d'
import CreateCategories from './components/createCategories'
import { ButtonPrimary } from '../../components/bootstrap/button'
import Card from '../../components/template/card'
import { showError } from '../../utils/notification'
import MainPanel from '../../components/template/mainPanel'
import { Column } from '../../components/bootstrap/layout'
import { useFetch } from '../../hooks/useFetch'
import UrgentPagination from '../../components/template/table/pagination'
import Spinner from '../../components/bootstrap/spinner'

const JobCategories: React.FC = () => {
  const [currentPage, setCurrentPage] = useState<number>(1)
  const [currentSize, setCurrentSize] = useState<number>(8)
  const [refresh, setRefresh] = useState<boolean>(false)
  const { data, isLoading } = useFetch<JobCategory[]>({
    initialData: [],
    reloadOn: [currentPage, currentSize, refresh],
    fetchFunction: async () => {
      return await getCategories({ limit: currentSize, offset: currentPage })
    }
  })
  const [modal, setModal] = useState<boolean>(false)
  const toggleModal = () => {
    setModal(!modal)
  }

  const handleSubmit = async (values: JobCategory) => {
    const [err] = await createCategories(values)
    if (err) {
      showError('Failed to create Category')
    } else {
      setRefresh(!refresh)
    }
  }

  return (
    <>
      <MainPanel title="Job Categories">
        <div className="row">
          <div className="col-sm-12">
            <ButtonPrimary title={'Create Category'} onClick={toggleModal} className="float-right mb-3"/>
            {/* <Table getData={getCategories} headers={headers} getCount={getCategoriesCount}/> */}
          </div>
        </div>
        <div className="row">
          {data.map((category) => {
            return (
              <Column key={category.id} className="col-sm-3 mt-3">
                <Card title={category.title}>
                  <i className={'la la-heart text-primary categories'}/>
                  <p className="fh fh-100">{category.description}</p>
                </Card>
              </Column>
            )
          })}
          {isLoading && <div className="col-sm-12"> <Spinner/></div>}
          {data.length === 0 && !isLoading && 'No Data'}
        </div>
        <UrgentPagination handlePageChange={setCurrentPage} hanldeSizeChange={setCurrentSize} getCount={getCategoriesCount} currentPage={currentPage} currentSize={currentSize}/>
      </MainPanel>
      {modal && <CreateCategories show={modal} toggleModal={toggleModal} submit={handleSubmit}/>}

    </>
  )
}

export default JobCategories

import React from 'react'
import { Switch, Redirect } from 'react-router-dom'
import PrivateRoute from '../../components/privateRoute'
import { DASHBOARD_ROUTE, CATEGORIES_ROUTE, ROLES_ROUTE, HOME_ROUTE, HEROES_ROUTE, USERS_ROUTE, HERO_DETAILS_ROUTE } from '../constants'
import { URGENT_UNBOARDING, URGENT_ADMIN, URGENT_SUPER_ADMIN } from '../../apis/users/constants'
import WithHeaderAndSideNav from '../../components/WithHeaderAndSideNav'
import FourOFour from '../../components/404'
import LoggedInRoute from '../../components/loggedInRoute'
import Spinner from '../../components/bootstrap/spinner'

const Users = React.lazy(() => import('../users'))
const JobCategories = React.lazy(() => import('../jobCategories'))
const RoleManagement = React.lazy(() => import('../roleManagement'))
const HeroRouter = React.lazy(() => import('../heroes/router'))
const Dashboard = React.lazy(() => import('../dashboard'))
const HeroDetails = React.lazy(() => import('../heroes/details'))
const PrivateRoutes: React.FC = () => {
  return (
    <WithHeaderAndSideNav>
      <React.Suspense fallback={<Spinner/>}>
        <Switch>
          <PrivateRoute path={HOME_ROUTE} exact render={() => <Redirect to={DASHBOARD_ROUTE}/>} role={URGENT_UNBOARDING}/>
          <PrivateRoute path={DASHBOARD_ROUTE} exact component={Dashboard} role={URGENT_UNBOARDING}/>
          <PrivateRoute path={USERS_ROUTE} exact component={Users} role={URGENT_UNBOARDING}/>
          <PrivateRoute path={CATEGORIES_ROUTE} exact component={JobCategories} role={URGENT_ADMIN}/>
          <PrivateRoute path={ROLES_ROUTE} exact component={RoleManagement} role={URGENT_SUPER_ADMIN}/>
          <PrivateRoute path={`${HERO_DETAILS_ROUTE}/:id`} exact component={HeroDetails} role={URGENT_UNBOARDING}/>
          <PrivateRoute path={HEROES_ROUTE} component={HeroRouter} role={URGENT_UNBOARDING}/>
          <LoggedInRoute component={FourOFour}/>
        </Switch>
      </React.Suspense>
    </WithHeaderAndSideNav>
  )
}

export default PrivateRoutes

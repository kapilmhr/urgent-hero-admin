import React from 'react'
import { Redirect } from 'react-router-dom'
import { useAuth, AuthHook } from '../../hooks/useAuth/useAuth'
import { roleRouteMap, UNAUTHORIZED_ROUTE } from '../constants'
import { CRouteComponentProps } from '../../@types/route'
import HTMLForm from '../../components/bootstrap/form'
import { ButtonPrimary } from '../../components/bootstrap/button'
import { Form } from 'react-final-form'
import { FinalInput } from '../../components/finalform/input'
import Card from '../../components/template/card'
import FullPageLoading from '../../components/fullPageLoading'
import { useProfile } from '../../hooks/useProfile'
import { URGENT_UNBOARDING, URGENT_ADMIN, URGENT_SUPER_ADMIN } from '../../apis/users/constants'

interface SignInSignUp{
  email: string;
  password: string;
}
const Login: React.FC<CRouteComponentProps> = ({ location }) => {
  const state = location.state as {from: string}
  const auth = useAuth() as AuthHook
  const onSubmit = async (values: SignInSignUp) => {
    await auth.signin(values.email, values.password)
  }
  const { user } = useProfile()
  const getRedirectRoute = () => {
    const { roles } = user.customClaims
    if (roles.includes(URGENT_UNBOARDING)) {
      return roleRouteMap[URGENT_UNBOARDING]
    } else if (roles.includes(URGENT_ADMIN)) {
      return roleRouteMap[URGENT_ADMIN]
    } else if (roles.includes(URGENT_SUPER_ADMIN)) {
      return roleRouteMap[URGENT_SUPER_ADMIN]
    }
    return UNAUTHORIZED_ROUTE
  }
  if (auth.wait) {
    return <FullPageLoading/>
  }
  if (!auth.user) {
    return (
      <div className="full-page-container">
        <div className="image-container">
          <img src="./images/urgent.png" alt="..." className="mx-auto d-block"/>
        </div>
        <Card title="Admin Panel">
          <Form
            onSubmit={onSubmit}
            render={
              ({ handleSubmit, submitting, pristine }) => {
                return (
                  <HTMLForm onSubmit={handleSubmit}>
                    <FinalInput name="email" title="Email"/>
                    <FinalInput name="password" title="Password" type="password"/>
                    <ButtonPrimary title="Sign In" type="submit" disabled={submitting || pristine} className="float-right"/>
                  </HTMLForm>
                )
              }
            }
          />
        </Card>
      </div>
    )
  }
  if (user && user.id !== '') {
    return <Redirect to={state && state.from && state.from !== '/' ? state.from : getRedirectRoute()}/>
  }
  return <FullPageLoading/>
}

export default Login

import { RouteComponentProps } from 'react-router-dom'

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface MatchParams {}
declare type CRouteComponentProps = RouteComponentProps<MatchParams>

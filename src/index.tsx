import React from 'react'
import ReactDOM from 'react-dom'
import App from './app'
import * as serviceWorker from './serviceWorker'
import './assets/styles/main.scss'
import { BrowserRouter } from 'react-router-dom'
import { ProvideAuth } from './hooks/useAuth/useAuth'
import { ProfileProvider } from './hooks/useProfile'
import { CategoryProvider } from './hooks/useCategories'
ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <ProvideAuth>
        <ProfileProvider>
          <CategoryProvider>
            <App />
          </CategoryProvider>
        </ProfileProvider>
      </ProvideAuth>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()

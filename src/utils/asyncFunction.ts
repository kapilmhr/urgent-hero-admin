/* eslint-disable @typescript-eslint/no-explicit-any */
import axios from '../apis/index'
import { AxiosRequestConfig } from 'axios'
import { getIdTokenFromLocalStorage } from './localStorage'

export declare type AxiosFunctionReturnType<T> = [any, T | null]

export const asyncFunction = async <T>(config: AxiosRequestConfig): Promise<AxiosFunctionReturnType<T>> => {
  try {
    const idToken = getIdTokenFromLocalStorage()
    const response = await axios({
      headers: {
        Authorization: `Bearer ${idToken}`
      },
      ...config
    })
    return [null, response.data]
  } catch (err) {
    return [err, null]
  }
}

import { NormalizedObject } from '../apis/types'

interface Base{
  id: string;
}
export const normalizeData = <T>(data: T[], idAttribure?: string) => {
  let normalizeData: NormalizedObject<T> = {}
  data.forEach((value: any) => {
    normalizeData = { ...normalizeData, [value[idAttribure || 'id']]: value }
  }
  )
  return normalizeData
}

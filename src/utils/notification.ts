import Noty from 'noty'

export const showNotification = (options: Noty.Options) => {
  const notification = new Noty({
    theme: 'bootstrap-v4',
    timeout: 5000,
    progressBar: false,
    ...options
  })
  notification.show()
  return notification
}

export const showSuccess = (text: string) => {
  return showNotification({ text, type: 'success' })
}

export const showError = (text: string) => {
  return showNotification({ text, type: 'error' })
}

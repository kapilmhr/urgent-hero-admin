
export const getLocalStorateItem = (itemKey: string) => {
  return localStorage.getItem(itemKey)
}

export const setLocalStorateItem = (itemKey: string, itemValue: string) => {
  return localStorage.setItem(itemKey, itemValue)
}

const localStorageToken = 'id_token'

export const getIdTokenFromLocalStorage = () => {
  return localStorage.getItem(localStorageToken)
}

export const setIdTokenToLocalStorage = (idToken: string) => {
  setLocalStorateItem(localStorageToken, idToken)
}

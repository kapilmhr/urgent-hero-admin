import moment from 'moment'

export const defaultFormat = 'll, HH:mm'
export const convertToDefault = (date: string|number) => {
  if (!date) {
    return '-'
  }
  return moment(date).format(defaultFormat)
}
